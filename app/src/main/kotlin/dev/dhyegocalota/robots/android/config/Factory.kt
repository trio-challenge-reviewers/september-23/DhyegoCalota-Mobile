package dev.dhyegocalota.robots.android.config

import android.content.Context
import dev.dhyegocalota.robots.engine.GameEngineBuilder
import dev.dhyegocalota.robots.engine.Logger

interface Factory {
    val context: Context
    val logger: Logger

    fun buildGameEngineBuilder(): GameEngineBuilder
}