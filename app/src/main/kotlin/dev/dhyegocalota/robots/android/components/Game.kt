package dev.dhyegocalota.robots.android.components

import ElapsedTime
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import dev.dhyegocalota.robots.android.data.GameState
import dev.dhyegocalota.robots.android.presentation.GameViewModel

@Composable
fun Game(modifier: Modifier = Modifier, state: GameState) {
    ConstraintLayout(modifier = modifier.fillMaxSize()) {
        val (elapsedTime, rank, map) = createRefs()

        Row(
            modifier = Modifier
                .constrainAs(elapsedTime) {
                    width = Dimension.fillToConstraints
                    start.linkTo(map.start)
                    end.linkTo(map.end)
                    bottom.linkTo(map.top)
                }
                .padding(vertical = 8.dp),
            horizontalArrangement = Arrangement.Center,
        ) {
            ElapsedTime(state = state)
        }

        Row(
            modifier = Modifier
                .constrainAs(rank) {
                    width = Dimension.fillToConstraints
                    start.linkTo(map.start)
                    end.linkTo(map.end)
                    bottom.linkTo(map.top)
                }
                .padding(vertical = 8.dp),
        ) {
            GameRank(state = state)
        }

        Row(
            modifier = Modifier
                .constrainAs(map) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .wrapContentWidth(Alignment.CenterHorizontally),
        ) {
            GameMap(
                state = state,
                cellSize = 40.dp,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GamePreview() {
    Game(state = GameViewModel.DEFAULT_GAME_STATE)
}