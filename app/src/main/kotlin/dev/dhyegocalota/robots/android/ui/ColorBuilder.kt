package dev.dhyegocalota.robots.android.ui

import androidx.compose.ui.graphics.Color

object ColorBuilder {
    fun build(hex: String): Color {
        return Color(android.graphics.Color.parseColor(hex))
    }
}