package dev.dhyegocalota.robots.android.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.dhyegocalota.robots.android.config.Factory
import dev.dhyegocalota.robots.android.data.GameState
import dev.dhyegocalota.robots.engine.Boundaries
import dev.dhyegocalota.robots.engine.Color
import dev.dhyegocalota.robots.engine.GameEngine
import dev.dhyegocalota.robots.engine.GreedyBotPlayer
import dev.dhyegocalota.robots.engine.RandomBotPlayer
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.ZERO

class GameViewModel(
    private val factory: Factory
) : ViewModel() {
    companion object {
        const val DEFAULT_SHOULD_DISPLAY_COORDINATE = false
        val DEFAULT_GAME_STATE = GameState(
            elapsedTime = ZERO,
            playersRank = mapOf(
                RandomBotPlayer(Color.PURPLE) to 0,
                GreedyBotPlayer(Color.GREEN) to 0
            ),
            playerCursors = emptySet(),
            playerStalemateMoves = emptyMap(),
            boundaries = Boundaries(width = 7, height = 7),
            cells = emptySet(),
            shouldDisplayCoordinate = DEFAULT_SHOULD_DISPLAY_COORDINATE
        )
    }

    private var engine: GameEngine
    private var _state = MutableStateFlow(DEFAULT_GAME_STATE)

    val state: StateFlow<GameState>
        get() = _state.asStateFlow()

    init {
        val buildGameEngineBuilder = factory.buildGameEngineBuilder()
        engine = buildGameEngineBuilder.build()
    }

    fun start() {
        viewModelScope.launch {
            engine.start(scope = viewModelScope).consumeEach { updateState() }
        }
    }

    private fun updateState() {
        factory.logger.debug("GameViewModel", "Updating State...")
        _state.value = buildState()
    }

    private fun buildState(): GameState {
        check(engine.currentGame != null) { "Game must be started before building state" }
        val currentGame = engine.currentGame!!
        val map = currentGame.map
        val playerStalemateMoves = engine.games.map { game ->
            game.playerCursors.map { playerCursor ->
                playerCursor.player.color to playerCursor.turnHistory.moves.count { it.stalemate }
            }
        }.flatten().toMap()

        return GameState(
            elapsedTime = engine.elapsedDuration,
            playersRank = engine.rank.playersRank,
            playerCursors = currentGame.playerCursors,
            playerStalemateMoves = playerStalemateMoves,
            boundaries = map.boundaries,
            cells = map.cells.toSet(),
            shouldDisplayCoordinate = DEFAULT_SHOULD_DISPLAY_COORDINATE,
        )
    }

    override fun onCleared() {
        super.onCleared()
        engine.stop()
    }
}