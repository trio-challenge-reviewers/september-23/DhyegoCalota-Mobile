import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import dev.dhyegocalota.robots.android.data.GameState
import dev.dhyegocalota.robots.android.presentation.GameViewModel

@Composable
fun ElapsedTime(modifier: Modifier = Modifier, state: GameState) {
    val elapsedTime = state.elapsedTime.toComponents { _, _, minutes, seconds, _ ->
        String.format("%02d:%02d", minutes, seconds)
    }

    Text(
        modifier = modifier,
        text = elapsedTime,
        fontSize = 18.sp,
        color = MaterialTheme.colorScheme.outline,
    )
}

@Preview
@Composable
fun ElapsedTimePreview() {
    ElapsedTime(state = GameViewModel.DEFAULT_GAME_STATE)
}