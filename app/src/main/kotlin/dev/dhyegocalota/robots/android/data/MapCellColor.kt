package dev.dhyegocalota.robots.android.data

import androidx.compose.ui.graphics.Color
import dev.dhyegocalota.robots.android.ui.ColorBuilder
import dev.dhyegocalota.robots.engine.MapCell
import dev.dhyegocalota.robots.engine.PlayerCursor

class MapCellColor(
    private val playerCursors: Set<PlayerCursor>,
    private val cell: MapCell?
) {
    fun build(): Color? {
        val playerCursor = playerCursors.firstOrNull { it.player == cell?.occupier }

        if (cell == null) {
            return null
        }

        if (playerCursor != null) {
            val playerColor = if (playerCursor.currentCoordinate == cell.coordinate) cell.color.activeHex else cell.color.nonActiveHex
            return ColorBuilder.build(playerColor)
        }

        return ColorBuilder.build(cell.color.nonActiveHex)
    }
}