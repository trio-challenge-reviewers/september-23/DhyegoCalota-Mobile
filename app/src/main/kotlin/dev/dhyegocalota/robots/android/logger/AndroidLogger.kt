package dev.dhyegocalota.robots.android.logger

import android.util.Log
import dev.dhyegocalota.robots.engine.Logger

class AndroidLogger : Logger {

    override fun verbose(tag: String, message: String) {
        Log.v(tag, message)
    }

    override fun debug(tag: String, message: String) {
        Log.d(tag, message)
    }

    override fun info(tag: String, message: String) {
        Log.i(tag, message)
    }

    override fun warning(tag: String, message: String) {
        Log.w(tag, message)
    }

    override fun error(tag: String, message: String) {
        Log.e(tag, message)
    }
}