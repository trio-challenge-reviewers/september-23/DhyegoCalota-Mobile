package dev.dhyegocalota.robots.android.data

import dev.dhyegocalota.robots.engine.Boundaries
import dev.dhyegocalota.robots.engine.Color
import dev.dhyegocalota.robots.engine.MapCell
import dev.dhyegocalota.robots.engine.Player
import dev.dhyegocalota.robots.engine.PlayerCursor
import kotlin.time.Duration

data class GameState(
    val elapsedTime: Duration,
    val playersRank: Map<Player, Int>,
    val playerCursors: Set<PlayerCursor>,
    val playerStalemateMoves: Map<Color, Int>,
    val boundaries: Boundaries,
    val cells: Set<MapCell>,
    val shouldDisplayCoordinate: Boolean
)