package dev.dhyegocalota.robots.android.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

fun <TViewModel : ViewModel> viewModelFactory(factory: () -> TViewModel) = object : ViewModelProvider.Factory {
    override fun <TViewModel : ViewModel> create(modelClass: Class<TViewModel>): TViewModel {
        return factory() as TViewModel
    }
}