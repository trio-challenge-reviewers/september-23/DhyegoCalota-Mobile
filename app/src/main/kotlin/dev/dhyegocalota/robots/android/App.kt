package dev.dhyegocalota.robots.android

import android.app.Application
import dev.dhyegocalota.robots.android.config.Factory
import dev.dhyegocalota.robots.android.config.ProductionFactory

class App : Application() {
    companion object {
        lateinit var factory: Factory
    }

    override fun onCreate() {
        super.onCreate()
        factory = ProductionFactory(this)
    }
}