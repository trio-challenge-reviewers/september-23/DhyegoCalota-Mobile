package dev.dhyegocalota.robots.android.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.dhyegocalota.robots.android.data.GameState
import dev.dhyegocalota.robots.android.data.MapCellColor
import dev.dhyegocalota.robots.engine.Coordinate

@Composable
fun GameMap(
    modifier: Modifier = Modifier,
    columnModifier: Modifier = Modifier,
    rowModifier: Modifier = Modifier,
    state: GameState,
    cellSize: Dp = 30.dp,
    cellSpacing: Dp = 4.dp,
) {
    Box(modifier = modifier) {
        LazyColumn(
            modifier = columnModifier,
            verticalArrangement = Arrangement.spacedBy(
                space = cellSpacing,
                alignment = Alignment.CenterVertically
            ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            items(state.boundaries.height) { rowIndex ->
                LazyRow(
                    modifier = rowModifier
                        .wrapContentWidth(Alignment.CenterHorizontally),
                    horizontalArrangement = Arrangement.spacedBy(
                        space = cellSpacing,
                        alignment = Alignment.CenterHorizontally
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    items(state.boundaries.width) { columnIndex ->
                        val coordinate = Coordinate(x = columnIndex, y = rowIndex)
                        val cell = state.cells.firstOrNull { it.coordinate == coordinate }
                        val color = MapCellColor(
                            playerCursors = state.playerCursors,
                            cell = cell
                        ).build()
                            ?: MaterialTheme.colorScheme.outline

                        GameMapCell(
                            modifier = Modifier.size(cellSize),
                            coordinate = coordinate,
                            color = color,
                            shouldDisplayCoordinate = state.shouldDisplayCoordinate
                        )
                    }
                }
            }
        }
    }
}