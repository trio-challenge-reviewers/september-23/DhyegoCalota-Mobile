package dev.dhyegocalota.robots.android.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.LayoutDirection
import dev.dhyegocalota.robots.android.data.GameState
import dev.dhyegocalota.robots.android.ui.ColorBuilder

@Composable
fun GameRank(
    modifier: Modifier = Modifier,
    playerModifier: Modifier = Modifier,
    state: GameState
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        state.playersRank.entries.forEachIndexed { index, (player, numberOfWins) ->
            val isIndexOdd = index % 2 == 0
            val direction = if (isIndexOdd) LayoutDirection.Ltr else LayoutDirection.Rtl
            val numberOfStalemateRounds = state.playerStalemateMoves[player.color] ?: 0

            PlayerRankStat(
                modifier = playerModifier,
                color = ColorBuilder.build(player.color.activeHex),
                numberOfWins = numberOfWins,
                numberOfStalemateRounds = numberOfStalemateRounds,
                direction = direction,
            )
        }
    }
}