package dev.dhyegocalota.robots.android.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import dev.dhyegocalota.robots.engine.Coordinate

@Composable
fun GameMapCell(
    modifier: Modifier = Modifier,
    coordinate: Coordinate,
    color: Color = MaterialTheme.colorScheme.outline,
    shouldDisplayCoordinate: Boolean = false,
) {
    Box(
        modifier = modifier
            .clip(CircleShape)
            .background(color),
        contentAlignment = Alignment.Center
    ) {
        if (shouldDisplayCoordinate) {
            Text(text = "${coordinate.x},${coordinate.y}")
        }
    }
}