package dev.dhyegocalota.robots.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import dev.dhyegocalota.robots.android.components.Game
import dev.dhyegocalota.robots.android.presentation.GameViewModel
import dev.dhyegocalota.robots.android.presentation.viewModelFactory
import dev.dhyegocalota.robots.android.ui.theme.RobotsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            RobotsTheme(dynamicColor = false, darkTheme = true) {
                val viewModel = viewModel<GameViewModel>(
                    factory = viewModelFactory {
                        GameViewModel(
                            factory = App.factory
                        )
                    }
                )

                val state by viewModel.state.collectAsState()
                viewModel.start()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Game(state = state)
                }
            }
        }
    }
}