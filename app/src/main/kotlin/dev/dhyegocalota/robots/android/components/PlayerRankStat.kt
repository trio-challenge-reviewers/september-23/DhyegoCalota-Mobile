package dev.dhyegocalota.robots.android.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun PlayerRankStat(
    modifier: Modifier = Modifier,
    circleModifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    direction: LayoutDirection,
    color: Color,
    numberOfWins: Int,
    numberOfStalemateRounds: Int,
) {
    CompositionLocalProvider(LocalLayoutDirection provides direction) {
        Row(
            modifier = modifier,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = circleModifier
                    .size(22.dp)
                    .clip(CircleShape)
                    .background(color)
            )

            Text(
                modifier = textModifier.padding(start = 4.dp),
                text = numberOfWins.toString(),
                fontSize = 18.sp,
                color = color
            )

            Text(
                modifier = textModifier.padding(start = 4.dp),
                text = "($numberOfStalemateRounds)",
                fontSize = 12.sp,
                color = MaterialTheme.colorScheme.outline,
            )
        }
    }
}

@Composable
@Preview
fun PlayerRankStatPreview() {
    PlayerRankStat(
        direction = LayoutDirection.Ltr,
        color = MaterialTheme.colorScheme.primary,
        numberOfWins = 1,
        numberOfStalemateRounds = 0,
    )
}