package dev.dhyegocalota.robots.android.config

import android.content.Context
import dev.dhyegocalota.robots.android.logger.AndroidLogger
import dev.dhyegocalota.robots.engine.Boundaries
import dev.dhyegocalota.robots.engine.Color
import dev.dhyegocalota.robots.engine.GameConfig
import dev.dhyegocalota.robots.engine.GameEngineBuilder
import dev.dhyegocalota.robots.engine.GreedyBotPlayer
import dev.dhyegocalota.robots.engine.Logger
import dev.dhyegocalota.robots.engine.RandomBotPlayer
import kotlin.time.Duration.Companion.milliseconds

class ProductionFactory(override val context: Context) : Factory {
    override val logger: Logger by lazy { AndroidLogger() }

    private val config = GameConfig(
        intervalSpeed = 500.milliseconds
    )

    private val boundaries = Boundaries(7, 7)

    private val numberOfPrizeTokens = 1

    override fun buildGameEngineBuilder(): GameEngineBuilder {
        return GameEngineBuilder()
            .withLogger(logger)
            .withConfig(config)
            .withBoundaries(boundaries)
            .withNumberOfPrizeTokens(numberOfPrizeTokens)
            .withPlayer(RandomBotPlayer(Color.PURPLE))
            .withPlayer(GreedyBotPlayer(Color.GREEN))
    }
}