package dev.dhyegocalota.robots.engine

import java.util.SortedSet

interface MapTransformer {
    fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell>
}