package dev.dhyegocalota.robots.engine

class WritableTurnHistory(override val initialCoordinate: Coordinate) : TurnHistory() {
    private val _history: MutableList<Pair<Turn, Move>> = mutableListOf()

    override val history: List<Pair<Turn, Move>>
        get() = _history.toList()

    fun add(turn: Turn, move: Move) {
        _history.add(turn to move)
    }
}