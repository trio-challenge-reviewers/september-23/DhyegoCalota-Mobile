package dev.dhyegocalota.robots.engine

data class WritablePlayerCursor(override val player: Player, override val map: Map) : PlayerCursor {
    override val initialCoordinate: Coordinate by lazy {
        map.cells.first { it.occupier == player }.coordinate
    }

    override val turnHistory: WritableTurnHistory by lazy {
        WritableTurnHistory(initialCoordinate)
    }

    override val currentCoordinate: Coordinate
        get() = turnHistory.currentCoordinate

    init {
        require(map.boundaries.isWithin(currentCoordinate)) { "Initial coordinate must be within the map" }
        require(map.cells.count { it.occupier == player } == 1) { "Player must occupy exactly one cell" }
    }

    override fun getMovableDirections(): Set<MoveDirection> {
        return MoveDirection.values.filter { direction ->
            val isMovable = map.boundaries.isMovable(currentCoordinate, direction)
            if (!isMovable) return@filter false

            val destinationCoordinate = direction.move(currentCoordinate)
            val destinationCell = map.getCell(destinationCoordinate)
            return@filter destinationCell.canBeOccupied()
        }.toSet()
    }

    internal fun move(turn: Turn, direction: MoveDirection?): Move {
        val move = Move(currentCoordinate, direction)
        turnHistory.add(turn, move)
        return move
    }
}