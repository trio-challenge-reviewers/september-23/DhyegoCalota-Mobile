package dev.dhyegocalota.robots.engine

import java.util.SortedSet

class WritableMap(initialCells: Set<MapCell>) :
    Map {
    private var _cells: MutableSet<MapCell> = initialCells.toMutableSet()

    override val boundaries: Boundaries by lazy { buildBoundaries() }

    override val cells: SortedSet<MapCell>
        get() = _cells.toSortedSet()

    override val coordinates: SortedSet<Coordinate>
        get() = cells.map { it.coordinate }.toSortedSet()

    init {
        require(initialCells.isNotEmpty()) { "Map must have at least one cell" }

        val expectedCoordinates = boundaries.buildCoordinates()
        require(expectedCoordinates.size == cells.size) { "Map must have all cells" }
        require(expectedCoordinates.containsAll(cells.map { it.coordinate })) { "Map must have all cells" }
    }

    private fun buildBoundaries(): Boundaries {
        val cellXs = _cells.map { it.coordinate.x }
        val cellYs = _cells.map { it.coordinate.y }
        val width = cellXs.isEmpty().let { if (it) 0 else cellXs.max()!! + 1 }
        val height = cellYs.isEmpty().let { if (it) 0 else cellYs.max()!! + 1 }

        return Boundaries(width, height)
    }

    override fun getCell(coordinate: Coordinate): MapCell {
        return _cells.first { it.coordinate == coordinate }
    }

    internal fun changeCell(cell: MapCell) {
        val transformer = CellReplacerMapTransformer(cell)
        _cells = transformer.transform(cells)
    }

    fun countPrizeTokens(): Int {
        return cells.count { it.isPrizeToken() }
    }

    fun countOccupiedCellsByPlayer(player: Player): Int {
        return cells.count { it.isOccupiedBy(player) }
    }

    internal fun movePlayer(turn: Turn, cursor: WritablePlayerCursor, direction: MoveDirection?): Coordinate {
        val currentCell = getCell(cursor.currentCoordinate)
        require(currentCell.isOccupiedBy(cursor.player)) { "Coordinate must be occupied by player" }

        if (direction != null) require(boundaries.isMovable(cursor.currentCoordinate, direction)) { "Move must be allowed" }

        val move = cursor.move(turn, direction)

        return move.destination.also { destination ->
            if (move.stalemate) return@also
            val targetCell = getCell(destination).let { it.occupy(cursor.player) }
            changeCell(targetCell)
        }
    }
}