package dev.dhyegocalota.robots.engine

import java.util.SortedSet

class AddPlayersToRandomCoordinatesMapTransformer(val playersToAdd: Set<Player>) : MapTransformer {
    private val remainingPlayersToAdd = playersToAdd.toMutableSet()

    init {
        require(remainingPlayersToAdd.isNotEmpty()) { "Number of players must be greater than zero" }
    }

    override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
        val map = WritableMap(cells)
        val nonOccupiedAndNonPrizeTokenCoordinates = map.cells.filter { it.isNonOccupiedAndNonPrizeToken() }.map { it.coordinate }
        require(nonOccupiedAndNonPrizeTokenCoordinates.isNotEmpty()) { "Number of players must be less than or equal to number of unoccupied cells" }

        val prizeCellCoordinates = map.cells.filter { it.isPrizeToken() }
            .map { it.coordinate }
            .toSet()

        val playerCellCoordinates = map.cells.filter { it.isOccupied() }
            .map { it.coordinate }
            .toSet()

        val playerOrPrizeCellCoordinates = prizeCellCoordinates.plus(playerCellCoordinates)

        val farthestCoordinatesFromOrigin = CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
            origins = setOf(Coordinate.INITIAL).plus(playerOrPrizeCellCoordinates),
            boundaries = map.boundaries,
            excluded = playerOrPrizeCellCoordinates,
        )

        val randomFarthestCoordinateFromOrigin = farthestCoordinatesFromOrigin.random()
        val farthestCellFromOrigin = map.getCell(randomFarthestCoordinateFromOrigin)
        val player = remainingPlayersToAdd.shuffled().first()
        val playerCell = farthestCellFromOrigin.occupy(player)
        val replacer = CellReplacerMapTransformer(playerCell)

        return replacer.transform(cells).let { partialTransformedCells ->
            remainingPlayersToAdd.remove(player)
            if (remainingPlayersToAdd.isNotEmpty()) return transform(partialTransformedCells)
            partialTransformedCells
        }
    }
}