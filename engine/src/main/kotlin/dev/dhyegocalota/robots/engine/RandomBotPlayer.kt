package dev.dhyegocalota.robots.engine

class RandomBotPlayer(override val color: Color) : Player(color) {
    override fun onTurn(turn: Turn, cursor: PlayerCursor) {}

    override suspend fun makeMove(turn: Turn, control: PlayerTurnControl) {
        val movableDirections = control.cursor.getMovableDirections()

        if (movableDirections.isEmpty()) {
            control.makeStalemateMove()
            return
        }

        val prizeToken = control.cursor.map.cells.first { it.isPrizeToken() }
        val currentCoordinate = control.cursor.currentCoordinate

        if (currentCoordinate == prizeToken.coordinate) {
            control.makeStalemateMove()
            return
        }

        val randomDirection = movableDirections.random()
        control.makeMove(randomDirection)
    }
}