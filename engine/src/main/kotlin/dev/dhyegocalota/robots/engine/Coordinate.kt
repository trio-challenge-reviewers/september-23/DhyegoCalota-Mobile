package dev.dhyegocalota.robots.engine

data class Coordinate(val x: Int, val y: Int) : Comparable<Coordinate> {
    companion object {
        val INITIAL = Coordinate(0, 0)

        fun build(coordinate: String): Coordinate {
            val coordinateXY = coordinate.split(",")
            require(coordinateXY.size == 2) { "Invalid coordinate: $coordinate" }
            val (x, y) = coordinateXY
            require(x.toIntOrNull() != null && y.toIntOrNull() != null) { "Invalid coordinate: $coordinate" }
            return Coordinate(x.toInt(), y.toInt())
        }
    }

    init {
        require(isValid()) { "Invalid coordinate: $this" }
    }

    private fun isValid() = x >= 0 && y >= 0

    override fun equals(otherCoordinate: Any?): Boolean {
        if (this === otherCoordinate) return true
        if (otherCoordinate is String) return this == build(otherCoordinate)
        if (otherCoordinate !is Coordinate) return false
        return this.x == otherCoordinate.x && this.y == otherCoordinate.y
    }

    override fun hashCode(): Int {
        return this.x.hashCode() + this.y.hashCode()
    }

    override fun compareTo(other: Coordinate): Int {
        if (this.x == other.x) {
            return this.y.compareTo(other.y)
        }
        return this.x.compareTo(other.x)
    }

    override fun toString(): String {
        return "$x,$y"
    }
}
