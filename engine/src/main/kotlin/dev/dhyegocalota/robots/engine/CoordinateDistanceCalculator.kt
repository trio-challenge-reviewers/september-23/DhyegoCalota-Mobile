package dev.dhyegocalota.robots.engine

import java.util.SortedSet
import kotlin.math.abs

object CoordinateDistanceCalculator {
    fun getDistance(origin: Coordinate, destination: Coordinate): Int {
        return abs(origin.x - destination.x) + abs(origin.y - destination.y)
    }

    fun getFarthestCoordinateWithinBoundaries(origins: Set<Coordinate>, boundaries: Boundaries, excluded: Set<Coordinate>? = null): SortedSet<Coordinate> {
        require(origins.isNotEmpty()) { "Origins must not be empty" }
        require(origins.all { boundaries.isWithin(it) }) { "Origins must be within boundaries" }
        require(excluded?.all { boundaries.isWithin(it) } ?: true) { "Excluded coordinates must be within boundaries" }

        val totalDistanceOfEachCoordinate = boundaries.buildCoordinates().filter { destination ->
            return@filter excluded?.contains(destination) == false
        }.associateWith { coordinate ->
            origins.map { origin -> origin to getDistance(origin, coordinate) }
        }

        val maximumTotalDistanceToAllOrigins = totalDistanceOfEachCoordinate.maxByOrNull { (_, distances) ->
            distances.sumOf { (_, distance) -> distance }
        }?.value?.sumOf { (_, distance) -> distance } ?: 0

        val potentialFarthestCoordinates = totalDistanceOfEachCoordinate.filter { (_, distances) ->
            distances.sumOf { (_, distance) -> distance } == maximumTotalDistanceToAllOrigins
        }.keys

        val diffToAvgOfAllOrigins = potentialFarthestCoordinates.map { coordinate ->
            val distancesToAllOrigins = totalDistanceOfEachCoordinate[coordinate]!!
            val averageDistanceToAllOrigins = distancesToAllOrigins.sumOf { (_, distance) -> distance } / origins.size
            val differenceBetweenAverageAndEachDistance = distancesToAllOrigins.map { (_, distance) ->
                distance - averageDistanceToAllOrigins
            }.map { abs(it) }.firstOrNull() ?: 0

            coordinate to differenceBetweenAverageAndEachDistance
        }

        val minDiffToAvgOfAllOrigins = diffToAvgOfAllOrigins.minByOrNull { (_, distance) ->
            distance
        }?.second ?: 0

        val farthestCoordinates = diffToAvgOfAllOrigins.filter { (_, distance) ->
            distance == minDiffToAvgOfAllOrigins
        }.map { (coordinate, _) -> coordinate }

        return farthestCoordinates.toSortedSet()
    }
}