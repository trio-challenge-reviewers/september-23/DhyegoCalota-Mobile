package dev.dhyegocalota.robots.engine

import java.util.SortedSet

class CellReplacerMapTransformer(
    private val cell: MapCell
) : MapTransformer {
    override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
        return cells.also {
            cells.first { it.coordinate == cell.coordinate }
                .let { existingCell -> cells.remove(existingCell) }
            cells.add(cell)
        }
    }
}