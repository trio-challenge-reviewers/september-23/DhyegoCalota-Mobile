package dev.dhyegocalota.robots.engine

import java.util.SortedSet

interface Map {
    val boundaries: Boundaries
    val cells: SortedSet<MapCell>
    val coordinates: SortedSet<Coordinate>

    fun getCell(coordinate: Coordinate): MapCell
}