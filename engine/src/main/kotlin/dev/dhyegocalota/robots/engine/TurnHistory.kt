package dev.dhyegocalota.robots.engine

abstract class TurnHistory {
    abstract val history: List<Pair<Turn, Move>>
    abstract val initialCoordinate: Coordinate
    val moves: List<Move>
        get() = history.map { it.second }

    val coordinates: List<Coordinate>
        get() = moves.fold(listOf(initialCoordinate)) { partialCoordinates, move ->
            partialCoordinates + move.destination
        }

    val currentCoordinate: Coordinate
        get() = coordinates.last()
}