package dev.dhyegocalota.robots.engine

sealed class MoveDirection(private val modifierX: Int, private val modifierY: Int) {
    companion object {
        val values: Set<MoveDirection>
            get() = setOf(UP, RIGHT, DOWN, LEFT)
    }

    object UP : MoveDirection(0, -1)
    object RIGHT : MoveDirection(1, 0)
    object DOWN : MoveDirection(0, 1)
    object LEFT : MoveDirection(-1, 0)

    fun move(coordinate: Coordinate) = Coordinate(coordinate.x + modifierX, coordinate.y + modifierY)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MoveDirection) return false
        if (modifierX != other.modifierX) return false
        if (modifierY != other.modifierY) return false
        return true
    }

    override fun hashCode(): Int {
        var result = modifierX
        result = 31 * result + modifierY
        return result
    }
}
