package dev.dhyegocalota.robots.engine

class GreedyBotPlayer(override val color: Color) : Player(color) {
    override fun onTurn(turn: Turn, cursor: PlayerCursor) {}

    override suspend fun makeMove(turn: Turn, control: PlayerTurnControl) {
        val movableDirections = control.cursor.getMovableDirections()

        if (movableDirections.isEmpty()) {
            control.makeStalemateMove()
            return
        }

        val prizeToken = control.cursor.map.cells.first { it.isPrizeToken() }
        val currentCoordinate = control.cursor.currentCoordinate

        if (currentCoordinate == prizeToken.coordinate) {
            control.makeStalemateMove()
            return
        }

        val distanceCoordinatesToPrizeToken = movableDirections.map { direction ->
            val moveCoordinate = direction.move(currentCoordinate)
            CoordinateDistanceCalculator.getDistance(
                origin = prizeToken.coordinate,
                destination = moveCoordinate,
            ) to direction
        }

        val shortestDistanceToPrizeToken = distanceCoordinatesToPrizeToken.minBy { it.first }.first
        val shorterDirectionsToPrizeToken = distanceCoordinatesToPrizeToken.filter { (distance, _) ->
            distance == shortestDistanceToPrizeToken
        }.map { it.second }


        val randomShortestDirection = shorterDirectionsToPrizeToken.random()
        control.makeMove(randomShortestDirection)
    }
}