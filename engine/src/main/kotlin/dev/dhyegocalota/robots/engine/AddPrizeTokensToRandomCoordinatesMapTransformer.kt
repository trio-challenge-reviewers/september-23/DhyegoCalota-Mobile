package dev.dhyegocalota.robots.engine

import java.util.SortedSet

class AddPrizeTokensToRandomCoordinatesMapTransformer(private var remainingNumberOfPrizeTokensToAdd: Int) : MapTransformer {

    init {
        require(remainingNumberOfPrizeTokensToAdd > 0) { "Number of prize tokens must be greater than 0" }
    }

    override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
        val map = WritableMap(cells)
        val nonOccupiedAndNonPrizeTokenCells = map.cells.filter { it.isNonOccupiedAndNonPrizeToken() }
        require(nonOccupiedAndNonPrizeTokenCells.isNotEmpty()) { "Number of prize tokens must be less than or equal to the number of unoccupied cells" }

        val playerOrPrizeCellCoordinates = map.cells.filter { it.isOccupied() || it.isPrizeToken() }
            .map { it.coordinate }
            .toSet()

        val potentialCells = map.cells.filter { playerOrPrizeCellCoordinates.contains(it.coordinate).not() }
            .map { it.coordinate }
            .toSortedSet()

        val randomCoordinate = potentialCells.random()
        val randomCell = map.getCell(randomCoordinate)
        val prizeTokenCell = randomCell.copy(type = MapCellType.PRIZE_TOKEN)
        val replacer = CellReplacerMapTransformer(prizeTokenCell)

        return replacer.transform(cells).let { partialTransformedCells ->
            remainingNumberOfPrizeTokensToAdd--
            if (remainingNumberOfPrizeTokensToAdd > 0) return transform(partialTransformedCells)
            partialTransformedCells
        }
    }
}