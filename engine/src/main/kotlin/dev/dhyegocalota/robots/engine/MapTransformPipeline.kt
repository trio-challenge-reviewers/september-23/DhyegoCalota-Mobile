package dev.dhyegocalota.robots.engine

import java.util.SortedSet

class MapTransformPipeline(private val transformers: Set<MapTransformer>) : MapTransformer {

    override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
        return transformers.fold(cells) { partialTransformedCells, transformer ->
            transformer.transform(partialTransformedCells)
        }
    }
}