package dev.dhyegocalota.robots.engine

abstract class Player(open val color: Color) {
    abstract fun onTurn(turn: Turn, cursor: PlayerCursor)

    abstract suspend fun makeMove(turn: Turn, control: PlayerTurnControl)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Player) return false
        return color == other.color
    }

    override fun hashCode(): Int = color.hashCode()
}
