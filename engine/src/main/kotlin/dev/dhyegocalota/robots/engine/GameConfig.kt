package dev.dhyegocalota.robots.engine

import kotlin.time.Duration

data class GameConfig(
    val intervalSpeed: Duration
) {
    companion object {
        val DEFAULT = GameConfig(
            intervalSpeed = TurnClock.DEFAULT_INTERVAL_SPEED
        )
    }
}
