package dev.dhyegocalota.robots.engine

import kotlinx.coroutines.withTimeout
import kotlin.time.Duration

abstract class TurnControl(private val players: Set<Player>, private val moveTimeout: Duration) : TurnClockObserver {
    init {
        require(players.isNotEmpty()) { "There must be at least one player" }
    }

    override suspend fun invoke(turn: Turn) {
        notifyPlayersTurn(turn)
        makePlayerMove(turn)
    }

    fun notifyPlayersTurn(turn: Turn) {
        players.forEach { player ->
            val cursor = buildCursor(player)
            player.onTurn(turn, cursor)
        }
    }

    suspend fun makePlayerMove(turn: Turn) {
        val player = getTurnPlayer(turn)

        withTimeout(moveTimeout) {
            val context = buildContext(turn, player)
            player.makeMove(turn, context)
        }
    }

    fun getTurnPlayer(turn: Turn): Player {
        return players.elementAt((turn.number - TurnClock.INITIAL_TURN.number) % players.size)
    }

    abstract fun buildContext(turn: Turn, player: Player): PlayerTurnControl

    abstract fun buildCursor(player: Player): PlayerCursor
}