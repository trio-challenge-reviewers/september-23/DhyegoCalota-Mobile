package dev.dhyegocalota.robots.engine

typealias GameWinnerObserver = (winner: Player) -> Unit