package dev.dhyegocalota.robots.engine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ClosedSendChannelException
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration

class GameEngine(
    private val logger: Logger,
    private val players: Set<Player>,
    private var mapBuilder: MapBuilder,
    private val onStart: GameObserver?,
    private val onWinner: GameWinnerObserver?,
    private val onStop: GameObserver?,
    private val onTurn: TurnClockObserver?,
    val config: GameConfig = GameConfig.DEFAULT)
{
    val games: MutableSet<Game> = mutableSetOf()

    val currentGame: Game?
        get() = games.lastOrNull()

    var isRunning = false
        private set

    val rank: GamesRank
        get() = GamesRank(games)

    var elapsedDuration: Duration = Duration.ZERO
        private set

    private var lastStartTime: Instant? = null

    private var currentChannel: Channel<Turn>? = null

    init {
        mapBuilder.also { builder ->
            players.forEach { player ->
                builder.withPlayer(player)
            }
        }
    }

    suspend fun start(scope: CoroutineScope, channel: Channel<Turn>? = null): Channel<Turn> {
        check(!isRunning) { "Game engine is already running" }

        val game = (currentGame == null || currentGame?.isGameOver == true)
            .let { if (it) addNewGame() else currentGame!! }

        check(!game.isRunning) { "Game is already running" }

        resetLastStartTime()

        return run(
            scope = scope,
            channel = channel,
            game = game
        ) {
            onStart?.invoke(game)
            it.start(scope)
        }
    }

    private fun addNewGame(): Game {
        val map = mapBuilder.build()

        return Game(
            logger = logger,
            map = map,
            players = players,
            onWinner = onWinner,
            onTurn = {
                onTurn?.invoke(it)
                computeElapsedDuration()
            },
            config = config,
        ).also {
            games.add(it)
        }
    }

    private fun resetLastStartTime() {
        lastStartTime = getTimeNow()
    }

    private fun getTimeNow() = Clock.System.now()

    private suspend fun run(scope: CoroutineScope, channel: Channel<Turn>? = null, game: Game, runGame: suspend (game: Game) -> Channel<Turn>): Channel<Turn> {
        isRunning = true
        currentChannel = channel ?: Channel()

        scope.launch {
            val turns = runGame.invoke(game)

            while (turns.isClosedForReceive.not()) {
                try {
                    val turn = turns.receive()
                    currentChannel?.send(turn)
                } catch (e: ClosedReceiveChannelException) {
                    break
                } catch (e: ClosedSendChannelException) {
                    return@launch
                }
            }
        }

        scope.launch {
            var shouldStartNextGame = false

            while (isRunning) {
                delay(game.clock.intervalSpeed.inWholeMilliseconds)

                if (game.isGameOver) {
                    stop(mustCloseChannel = false)
                    shouldStartNextGame = true
                    break
                }
            }

            if (shouldStartNextGame) {
                start(
                    scope = scope,
                    channel = currentChannel
                )
            }
        }

        return currentChannel!!
    }

    fun stop() {
        stop(mustCloseChannel = true)
    }

    private fun stop(mustCloseChannel: Boolean = true) {
        if (!isRunning) return
        isRunning = false

        val game = currentGame!!
        game.stop()

        if (mustCloseChannel) closeChannel()

        onStop?.invoke(game)
        computeElapsedDuration()
    }

    private fun closeChannel() {
        check(currentChannel != null) { "Game engine is not running" }
        currentChannel!!.close()
        currentChannel = null
    }

    private fun computeElapsedDuration() {
        check(lastStartTime != null) { "Last start time must not be null" }
        val now = getTimeNow()
        val elapsedTime = now - lastStartTime!!
        elapsedDuration += elapsedTime
        resetLastStartTime()
    }
}