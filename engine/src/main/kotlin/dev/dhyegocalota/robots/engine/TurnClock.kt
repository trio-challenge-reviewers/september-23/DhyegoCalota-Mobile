package dev.dhyegocalota.robots.engine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

class TurnClock(private val logger: Logger, val intervalSpeed: Duration = DEFAULT_INTERVAL_SPEED) {
    companion object {
        val DEFAULT_INTERVAL_SPEED = 100.milliseconds
        val INITIAL_TURN = Turn()
        private const val LOGGER_TAG = "TurnClock"
    }

    private var _currentTurn: Turn = INITIAL_TURN
    private var _isRunning = false
    private val observers = mutableSetOf<TurnClockObserver>()

    init {
        require(intervalSpeed.inWholeMilliseconds > 0) { "Interval speed must be greater than 0" }

        if (intervalSpeed.inWholeMilliseconds < 100) {
            logger.warning(LOGGER_TAG, "Interval speed is too low, it may cause performance issues")
        }
    }

    val isRunning: Boolean
        get() = _isRunning

    val currentTurn: Turn
        get() = _currentTurn

    val didStart: Boolean
        get() = _currentTurn.number > INITIAL_TURN.number

    suspend fun start(scope: CoroutineScope): Channel<Turn> {
        check(!_isRunning) { "Turn clock is already running" }
        _isRunning = true
        if (didStart) _currentTurn = _currentTurn.next()
        return run(scope)
    }

    private suspend fun run(scope: CoroutineScope): Channel<Turn> {
        return Channel<Turn>().also { channel ->
            scope.launch {
                while (_isRunning) {
                    nextTurn(scope) {
                        channel.send(_currentTurn)
                        delay(intervalSpeed.inWholeMilliseconds)
                    }
                }
            }
        }
    }

    internal suspend fun nextTurn(scope: CoroutineScope, onBeforeNextTurn: (suspend (turn: Turn) -> Unit)? = null) {
        logger.info(LOGGER_TAG, "Turn ${_currentTurn.number}")
        notifyObservers(scope)
        onBeforeNextTurn?.invoke(_currentTurn)
        _currentTurn = _currentTurn.next()
    }

    fun stop() {
        _isRunning = false
    }

    fun addObserver(observer: TurnClockObserver) {
        observers.add(observer)
    }

    fun removeObserver(observer: TurnClockObserver) {
        observers.remove(observer)
    }

    private suspend fun notifyObservers(scope: CoroutineScope) {
        // TODO: Do we need to run this in parallel?
        observers.forEach { observer ->
            observer(_currentTurn)
        }
    }
}
