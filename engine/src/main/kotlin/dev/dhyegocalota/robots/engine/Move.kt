package dev.dhyegocalota.robots.engine

class Move(val origin: Coordinate, val direction: MoveDirection?) {
    val stalemate: Boolean = direction == null
    val destination: Coordinate = direction?.move(origin) ?: origin

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Move) return false
        if (origin != other.origin) return false
        if (direction != other.direction) return false
        if (stalemate != other.stalemate) return false
        if (destination != other.destination) return false
        return true
    }

    override fun hashCode(): Int {
        var result = origin.hashCode()
        result = 31 * result + (direction?.hashCode() ?: 0)
        result = 31 * result + stalemate.hashCode()
        result = 31 * result + destination.hashCode()
        return result
    }
}