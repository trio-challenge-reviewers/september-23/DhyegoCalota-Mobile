package dev.dhyegocalota.robots.engine

class GameEndTurnClockObserver(private val map: Map, private val onWinner: GameWinnerObserver) : TurnClockObserver {
    var winner: Player? = null
        private set

    val isGameOver: Boolean
        get() = winner != null

    override suspend fun invoke(turn: Turn) {
        determineWinner()
    }

    private suspend fun determineWinner() {
        if (isGameOver) return

        val capturedPrizeTokenCell = map.cells.firstOrNull() { it.isPrizeToken() && it.isOccupied() }
            ?: return

        winner = capturedPrizeTokenCell.occupier
        onWinner(winner!!)
    }
}