package dev.dhyegocalota.robots.engine

class MapBuilder {
    companion object {
        val DEFAULT_MAP_CELL_TYPE = MapCellType.LAND
        const val DEFAULT_NUMBER_OF_PRIZE_TOKENS = 1
    }

    private val players = mutableSetOf<Player>()
    private var numberOfPrizeTokens = DEFAULT_NUMBER_OF_PRIZE_TOKENS
    private lateinit var boundaries: Boundaries

    fun withPlayer(player: Player): MapBuilder {
        players.add(player)
        return this
    }

    fun withBoundaries(boundaries: Boundaries): MapBuilder {
        this.boundaries = boundaries
        return this
    }

    fun withNumberOfPrizeTokens(numberOfPrizeTokens: Int): MapBuilder {
        this.numberOfPrizeTokens = numberOfPrizeTokens
        return this
    }

    fun build(): WritableMap {
        require(players.isNotEmpty()) { "At least one player is required" }
        require(::boundaries.isInitialized) { "Boundaries is required" }
        require(numberOfPrizeTokens > 0) { "Number of prize tokens must be greater than 0" }

        return WritableMap(
            initialCells = buildInitialCells(),
        )
    }

    private fun buildInitialCells(): Set<MapCell> {
        val coordinates = boundaries.buildCoordinates()
        val cells = coordinates.map { coordinate ->
            MapCell(
                coordinate = coordinate,
                type = DEFAULT_MAP_CELL_TYPE
            )
        }.toSortedSet()

        val transformerPipeline = MapTransformPipeline(
            transformers = setOf(
                AddPrizeTokensToRandomCoordinatesMapTransformer(numberOfPrizeTokens),
                AddPlayersToRandomCoordinatesMapTransformer(players)
            )
        )

        return transformerPipeline.transform(cells)
    }
}