package dev.dhyegocalota.robots.engine

sealed class MapCellType(private val colorBuilder: (occupier: Player?) -> Color) {
    object PRIZE_TOKEN : MapCellType({ Color.YELLOW })
    object LAND : MapCellType({ occupier -> occupier?.color ?: Color.GRAY })

    fun buildColor(occupier: Player?): Color = colorBuilder(occupier)

    val isPrizeToken: Boolean
        get() = this == PRIZE_TOKEN

    val isLand: Boolean
        get() = this == LAND
}