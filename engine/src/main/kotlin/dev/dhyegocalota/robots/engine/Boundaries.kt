package dev.dhyegocalota.robots.engine

import java.util.SortedSet

data class Boundaries(
    val width: Int,
    val height: Int
) {
    companion object {
        fun build(boundaries: String): Boundaries {
            val boundariesWidthAndHeight = boundaries.split("x")
            require(boundariesWidthAndHeight.size == 2) { "Invalid boundaries: $boundaries" }
            val (width, height) = boundariesWidthAndHeight
            require(width.toIntOrNull() != null && height.toIntOrNull() != null) { "Invalid boundaries: $boundaries" }
            return Boundaries(width.toInt(), height.toInt())
        }
    }

    init {
        require(width >= 0 && height >= 0) { "Invalid boundaries: $this" }
    }

    fun countCells(): Int {
        return width * height
    }

    fun buildCoordinates(): SortedSet<Coordinate> {
        return (0 until width).flatMap { x ->
            (0 until height).map { y ->
                Coordinate(x, y)
            }
        }.toSortedSet()
    }

    fun isWithin(coordinate: Coordinate): Boolean {
        return coordinate.x < width && coordinate.y < height
    }

    fun isMovable(sourceCoordinate: Coordinate, direction: MoveDirection): Boolean {
        return try {
            val targetCoordinate = direction.move(sourceCoordinate)
            isWithin(targetCoordinate)
        } catch (e: IllegalArgumentException) {
            false
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Boundaries) return false
        return width == other.width && height == other.height
    }

    override fun hashCode(): Int {
        var result = width
        result = 31 * result + height
        return result
    }

    override fun toString(): String {
        return "${width}x${height}"
    }
}
