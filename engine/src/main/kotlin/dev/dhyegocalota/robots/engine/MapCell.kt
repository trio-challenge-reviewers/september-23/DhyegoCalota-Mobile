package dev.dhyegocalota.robots.engine

data class MapCell(val type: MapCellType, val coordinate: Coordinate, val occupier: Player? = null) : Comparable<MapCell> {

    val color: Color
        get() = type.buildColor(occupier)

    fun isEmpty() = !isOccupied()

    fun isPrizeToken() = type.isPrizeToken

    fun isLand() = type.isLand

    fun isOccupied() = occupier != null

    fun isOccupiedBy(player: Player) = isOccupied() && occupier == player

    fun canBeOccupied() = !isOccupied()

    fun occupy(player: Player): MapCell {
        check(canBeOccupied()) { "Cell must be empty to be occupied" }
        return MapCell(type, coordinate, player)
    }

    fun isNonOccupiedAndNonPrizeToken() = !isOccupied() && !isPrizeToken()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MapCell) return false
        if (coordinate != other.coordinate) return false
        if (type != other.type) return false
        if (occupier != other.occupier) return false
        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + coordinate.hashCode()
        result = 31 * result + (occupier?.hashCode() ?: 0)
        return result
    }

    override fun compareTo(other: MapCell): Int {
        return coordinate.compareTo(other.coordinate)
    }
}
