package dev.dhyegocalota.robots.engine

typealias TurnClockObserver = suspend (turn: Turn) -> Unit