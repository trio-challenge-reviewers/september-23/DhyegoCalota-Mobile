package dev.dhyegocalota.robots.engine

interface PlayerTurnControl {
    val cursor: PlayerCursor

    suspend fun makeMove(direction: MoveDirection): Unit

    suspend fun makeStalemateMove(): Unit
}