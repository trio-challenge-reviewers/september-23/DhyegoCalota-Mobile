package dev.dhyegocalota.robots.engine

class Turn(val number: Int = INITIAL_TURN_NUMBER) {
    companion object {
        private const val INITIAL_TURN_NUMBER = 1
    }

    fun isFirst() = number == INITIAL_TURN_NUMBER

    fun next() = Turn(number + 1)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Turn) return false
        if (number != other.number) return false
        return true
    }

    override fun hashCode(): Int {
        return number.hashCode()
    }

    override fun toString(): String {
        return "$number"
    }
}
