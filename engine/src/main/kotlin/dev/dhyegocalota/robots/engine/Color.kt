package dev.dhyegocalota.robots.engine

sealed class Color(val isReserved: Boolean, val nonActiveHex: String, val activeHex: String) {
    object PURPLE : Color(
        isReserved = false,
        nonActiveHex = "#590D88",
        activeHex = "#A220F6"
    )

    object GREEN : Color(
        isReserved = false,
        nonActiveHex = "#39806D",
        activeHex = "#71F3CD"
    )

    object YELLOW : Color(
        isReserved = true,
        nonActiveHex = "#F2A93B",
        activeHex = "#AB6C0E"
    )

    object RED : Color(
        isReserved = false,
        nonActiveHex = "#D91E1E",
        activeHex = "#F63A3A"
    )

    object BLUE : Color(
        isReserved = false,
        nonActiveHex = "#1E6ED9",
        activeHex = "#3A9BF6"
    )

    object MAGENTA : Color(
        isReserved = false,
        nonActiveHex = "#D91ED9",
        activeHex = "#F63AF6"
    )

    object CYAN : Color(
        isReserved = false,
        nonActiveHex = "#1ED9D9",
        activeHex = "#3AF6F6"
    )

    object PINK : Color(
        isReserved = false,
        nonActiveHex = "#D91E6E",
        activeHex = "#F63A9B"
    )

    object GRAY : Color(
        isReserved = false,
        nonActiveHex = "#D8D8D8",
        activeHex = "#D0D0D0"
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Color) return false
        return nonActiveHex == other.nonActiveHex && activeHex == other.activeHex
    }
    override fun hashCode(): Int {
        var result = nonActiveHex.hashCode()
        result = 31 * result + activeHex.hashCode()
        return result
    }
}
