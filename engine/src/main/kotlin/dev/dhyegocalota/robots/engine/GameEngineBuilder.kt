package dev.dhyegocalota.robots.engine

class GameEngineBuilder {
    private lateinit var logger: Logger

    private val players = mutableSetOf<Player>()
    private var numberOfPrizeTokens = MapBuilder.DEFAULT_NUMBER_OF_PRIZE_TOKENS
    private lateinit var boundaries: Boundaries

    private var onStart: GameObserver? = null
    private var onWinner: GameWinnerObserver? = null
    private var onStop: GameObserver? = null
    private var onTurn: TurnClockObserver? = null

    private var config: GameConfig = GameConfig.DEFAULT

    fun withLogger(logger: Logger) = apply { this.logger = logger }

    fun withPlayer(player: Player) = apply { players.add(player) }

    fun withNumberOfPrizeTokens(numberOfPrizeTokens: Int) = apply { this.numberOfPrizeTokens = numberOfPrizeTokens }

    fun withBoundaries(boundaries: Boundaries) = apply { this.boundaries = boundaries }

    fun withOnStart(onStart: GameObserver) = apply { this.onStart = onStart }

    fun withOnWinner(onWinner: GameWinnerObserver) = apply { this.onWinner = onWinner }

    fun withOnStop(onStop: GameObserver) = apply { this.onStop = onStop }

    fun withOnTurn(onTurn: TurnClockObserver) = apply { this.onTurn = onTurn }

    fun withConfig(config: GameConfig) = apply { this.config = config }

    fun build(): GameEngine {
        check(::logger.isInitialized) { "Logger is required" }
        check(::boundaries.isInitialized) { "Boundaries is required" }

        val mapBuilder = MapBuilder()
            .withBoundaries(boundaries)
            .withNumberOfPrizeTokens(numberOfPrizeTokens)

        return GameEngine(
            logger = logger!!,
            players = players,
            mapBuilder = mapBuilder!!,
            onStart = onStart,
            onWinner = onWinner,
            onStop = onStop,
            onTurn = onTurn,
            config = config
        )
    }
}