package dev.dhyegocalota.robots.engine

import kotlin.collections.Map

class GamesRank(val games: Set<Game>) {

    val players: Set<Player>
        get() = games.flatMap { it.players }.toSet()

    val playersRank: Map<Player, Int>
        get() = players
            .map { player ->
                val playerGames = games.filter { it.players.contains(player) }
                val playerWins = playerGames.count { it.winner == player }
                player to playerWins
            }
            .toMap()

    val playersRankSorted: List<Pair<Player, Int>>
        get() = playersRank.toList().sortedByDescending { (_, wins) -> wins }
}