package dev.dhyegocalota.robots.engine

typealias GameObserver = (Game) -> Unit