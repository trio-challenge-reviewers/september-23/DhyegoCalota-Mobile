package dev.dhyegocalota.robots.engine

interface PlayerCursor {
    val player: Player
    val map: Map
    val turnHistory: TurnHistory
    val initialCoordinate: Coordinate
    val currentCoordinate: Coordinate

    fun getMovableDirections(): Set<MoveDirection>
}