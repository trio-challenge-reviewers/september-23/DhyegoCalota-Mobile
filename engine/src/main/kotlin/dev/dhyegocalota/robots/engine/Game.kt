package dev.dhyegocalota.robots.engine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel

class Game(logger: Logger, val map: WritableMap, val players: Set<Player>, private val onWinner: GameWinnerObserver? = null, private val onTurn: TurnClockObserver? = null, val config: GameConfig = GameConfig.DEFAULT) {
    val clock = TurnClock(logger = logger, intervalSpeed = config.intervalSpeed)

    val playerCursors: Set<PlayerCursor>
        get() = _playerCursors

    val isRunning: Boolean
        get() = clock.isRunning

    var winner: Player? = null
        private set

    val isGameOver: Boolean
        get() = winner != null

    private val _playerCursors: Set<WritablePlayerCursor> by lazy {
        players.map { player -> WritablePlayerCursor(player, map) }.toSet()
    }

    private val determineEnd by lazy {
        GameEndTurnClockObserver(
            map = map,
            onWinner = { winner ->
                stop()
                this.winner = winner
                onWinner?.invoke(winner)
            }
        )
    }

    private val move by lazy {
        object : TurnControl(players = players, moveTimeout = config.intervalSpeed) {
            override fun buildContext(turn: Turn, player: Player): PlayerTurnControl {
                return object : PlayerTurnControl {
                    private val writableCursor: WritablePlayerCursor
                        get() = _playerCursors.first { it.player == player }

                    override val cursor: PlayerCursor
                        get() = writableCursor

                    override suspend fun makeMove(direction: MoveDirection) {
                        map.movePlayer(
                            turn = turn,
                            cursor = writableCursor,
                            direction = direction
                        )
                    }

                    override suspend fun makeStalemateMove() {
                        map.movePlayer(
                            turn = turn,
                            cursor = writableCursor,
                            direction = null
                        )
                    }
                }
            }

            override fun buildCursor(player: Player): PlayerCursor {
                return playerCursors.first { it.player == player }
            }
        }
    }

    init {
        require(players.isNotEmpty()) { "Map must have at least one player" }
        require(players.all { it.color.isReserved.not() }) { "Players cannot have reserved colors" }
        require(map.countPrizeTokens() > 0) { "Map must have at least one prize token" }
        require(players.all { map.countOccupiedCellsByPlayer(it) == 1 }) { "Each player must have one occupied cell" }

        clock.addObserver(::handleTurn)
        clock.addObserver(move)
        clock.addObserver(determineEnd)
    }

    private suspend fun handleTurn(turn: Turn) {
        onTurn?.invoke(turn)
    }

    suspend fun start(scope: CoroutineScope): Channel<Turn> {
        check(winner == null) { "Game is already over" }
        return clock.start(scope)
    }

    fun stop() {
        clock.stop()
    }

    internal fun movePlayer(turn: Turn, player: Player, direction: MoveDirection): Coordinate {
        require(players.contains(player)) { "Player must be part of map" }

        val cursor = _playerCursors.first { it.player == player }
        return map.movePlayer(turn, cursor, direction)
    }
}