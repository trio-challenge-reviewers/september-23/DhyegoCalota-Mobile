package dev.dhyegocalota.robots.engine.helper

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.runBlocking

object ChannelReader {
    suspend fun <T> read(channel: Channel<T>, times: Int? = null, onAfter: ((messages: List<T>) -> Unit)? = null): List<T> = runBlocking {
        val list = mutableListOf<T>()
        var coroutine: Deferred<Unit>? = times.let {
            if (times == null) {
                return@let async {
                    while (channel.isClosedForReceive.not()) {
                        list.add(channel.receive())
                    }
                }
            }

            return@let async {
                repeat(times) {
                    try {
                        list.add(channel.receive())
                    } catch (e: ClosedReceiveChannelException) {
                        return@async
                    }
                }
            }
        }

        coroutine?.await()
        onAfter?.invoke(list)
        return@runBlocking list
    }
}