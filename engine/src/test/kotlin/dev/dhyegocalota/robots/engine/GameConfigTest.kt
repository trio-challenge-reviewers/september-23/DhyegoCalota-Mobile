package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test
import kotlin.time.Duration.Companion.milliseconds

class GameConfigTest {
    
    @Test
    fun constructor_shouldReturnGameConfig() {
        val gameConfig = GameConfig(
            intervalSpeed = 1000.milliseconds,
        )

        Assert.assertEquals(1000.milliseconds, gameConfig.intervalSpeed)
    }
    
    @Test
    fun constructor_shouldReturnGameConfigWithDefaultValues() {
        val gameConfig = GameConfig.DEFAULT

        Assert.assertEquals(100.milliseconds, gameConfig.intervalSpeed)
    }
}