package dev.dhyegocalota.robots.engine.config

import dev.dhyegocalota.robots.engine.Map
import dev.dhyegocalota.robots.engine.MoveDirection
import dev.dhyegocalota.robots.engine.Player
import dev.dhyegocalota.robots.engine.PlayerCursor
import dev.dhyegocalota.robots.engine.PlayerTurnControl
import dev.dhyegocalota.robots.engine.Turn
import dev.dhyegocalota.robots.engine.TurnControl
import dev.dhyegocalota.robots.engine.WritablePlayerCursor
import kotlin.time.Duration

class NoOpTurnControl(private val map: Map, players: Set<Player>, moveTimeout: Duration) : TurnControl(players, moveTimeout) {
    override fun buildContext(turn: Turn, player: Player): PlayerTurnControl {
        return object : PlayerTurnControl {
            override val cursor: PlayerCursor
                get() = buildCursor(player)

            override suspend fun makeMove(direction: MoveDirection) {}

            override suspend fun makeStalemateMove() {}
        }
    }

    override fun buildCursor(player: Player): PlayerCursor {
        return WritablePlayerCursor(player, map)
    }
}