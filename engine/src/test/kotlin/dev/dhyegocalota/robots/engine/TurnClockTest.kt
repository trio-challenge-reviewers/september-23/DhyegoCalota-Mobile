package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import dev.dhyegocalota.robots.engine.helper.ChannelReader
import dev.dhyegocalota.robots.engine.helper.CoroutineScopeBuilder
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import kotlin.time.Duration.Companion.milliseconds

class TurnClockTest {

    private lateinit var logger: Logger
    private lateinit var turnClock: TurnClock

    @Before
    fun setUp() {
        logger = PrintLnLogger()
        turnClock = TurnClock(logger = logger)
    }

    @Test
    fun constructor_whenIntervalSpeedIsNegative_shouldThrowIllegalArgumentException() {
        val exception = kotlin.runCatching {
            TurnClock(intervalSpeed = -1.milliseconds, logger = logger)
        }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            exception.getOrThrow()
        }
    }

    @Test
    fun currentTurn_whenGameIsNotStarted_shouldReturnInitialTurn() {
        Assert.assertEquals(TurnClock.INITIAL_TURN.number, turnClock.currentTurn.number)
    }

    @Test
    fun currentTurn_whenGameIsStarted_shouldReturnCurrentTurn() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        val turns = ChannelReader.read(turnsChannel, 1)

        turnClock.stop()

        Assert.assertEquals(1, turns.first().number)
        Assert.assertEquals(1, turnClock.currentTurn.number)
    }

    @Test
    fun isRunning_whenGameIsNotStarted_shouldReturnFalse() {
        Assert.assertFalse(turnClock.isRunning)
    }

    @Test
    fun isRunning_whenGameIsStarted_shouldReturnTrue() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        val wasRunning = turnClock.isRunning

        ChannelReader.read(turnsChannel, 1) {
            turnClock.stop()
        }

        Assert.assertTrue(wasRunning)
    }

    @Test
    fun start_whenGameIsNotStarted_shouldStartGame() = runTest {
        val turnCount = 2
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        val turns = ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertEquals(listOf(1, 2), turns.map { it.number })
    }

    @Test
    fun start_whenGameIsStarted_shouldNotStartGame() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)

        val turnException = kotlin.runCatching {
            turnClock.start(gameScope)
        }

        turnClock.stop()

        Assert.assertThrows(IllegalStateException::class.java) {
            turnException.getOrThrow()
        }
    }

    @Test
    fun stop_whenGameIsNotStarted_shouldNotThrowException() = runTest {
        val turnException = kotlin.runCatching {
            turnClock.stop()
        }

        Assert.assertFalse(turnException.isFailure)
    }

    @Test
    fun stop_whenGameIsStarted_shouldStopGame() = runTest {
        val turnCount = 2
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        val turns = ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertFalse(turnClock.isRunning)
        Assert.assertEquals(listOf(1, 2), turns.map { it.number })
        Assert.assertEquals(2, turnClock.currentTurn.number)
    }

    @Test
    fun start_whenGameIsStopped_shouldNotResetTurn() = runTest {
        val turnCount = 2
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        var turnsChannel = turnClock.start(gameScope)
        val turnsBeforeStop = ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()
        turnsChannel = turnClock.start(gameScope)

        val turnsAfterStop = ChannelReader.read(turnsChannel, turnCount)
        turnClock.stop()

        Assert.assertEquals(Turn(2), turnsBeforeStop.last())
        Assert.assertEquals(Turn(3), turnsAfterStop.first())
    }

    @Test
    fun addObserver_shouldAddObserver() = runTest {
        val turnCount = 1
        var observerCalled = false
        val observer: TurnClockObserver = {
            observerCalled = true
        }

        turnClock.addObserver(observer)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertTrue(observerCalled)
    }

    @Test
    fun addObserver_whenObserverIsAlreadyAdded_shouldNotDuplicateObserver() = runTest {
        val turnCount = 1
        var numberOfObserverCalls = 0
        val observer: TurnClockObserver = {
            numberOfObserverCalls++
        }

        turnClock.addObserver(observer)
        turnClock.addObserver(observer)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertEquals(1, numberOfObserverCalls)
    }

    @Test
    fun addObserver_whenMultipleObserversAreAdded_shouldAddAllObservers() = runTest {
        val turnCount = 1
        var numberOfObserverCalls = 0
        val observer1: TurnClockObserver = {
            numberOfObserverCalls++
        }

        val observer2: TurnClockObserver = {
            numberOfObserverCalls++
        }

        val observer3: TurnClockObserver = {
            numberOfObserverCalls++
        }

        turnClock.addObserver(observer1)
        turnClock.addObserver(observer2)
        turnClock.addObserver(observer3)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertEquals(3, numberOfObserverCalls)
    }

    @Test
    @Ignore("TODO: It's unclear if we need to ensure no delay on clock turns.")
    fun addObserver_whenObserverTakesLongerThanIntervalSpeed_shouldNotDelayNextTurn() = runTest {
        val turnCount = 2
        val observer: TurnClockObserver = {
            delay(1000)
        }

        turnClock.addObserver(observer)

        val startTime = System.currentTimeMillis()
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        val turns = ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()
        val endTime = System.currentTimeMillis()
        val timeElapsed = endTime - startTime

        Assert.assertEquals(2, turns.size)
        Assert.assertTrue(timeElapsed < turnCount * turnClock.intervalSpeed.inWholeMilliseconds)
    }

    @Test
    fun removeObserver_shouldRemoveObserver() = runTest {
        val turnCount = 1
        var observerCalled = false
        val observer: TurnClockObserver = {
            observerCalled = true
        }

        turnClock.addObserver(observer)
        turnClock.removeObserver(observer)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertFalse(observerCalled)
    }

    @Test
    fun removeObserver_whenObserverIsNotAdded_shouldNotRemoveObserver() = runTest {
        val turnCount = 1
        var observerCalled = false
        val observer: TurnClockObserver = {
            observerCalled = true
        }

        turnClock.removeObserver(observer)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = turnClock.start(gameScope)
        ChannelReader.read(turnsChannel, turnCount)

        turnClock.stop()

        Assert.assertFalse(observerCalled)
    }

    @Test
    fun nextTurn_shouldIncrementCurrentTurn() = runTest {
        val maxNumberOfTurns = 5
        val gameScope = CoroutineScopeBuilder.buildGameScope()

        repeat(maxNumberOfTurns) {index ->
            var expectedTurnNumber = index + 1

            turnClock.nextTurn(gameScope) {
                Assert.assertEquals(expectedTurnNumber, turnClock.currentTurn.number)
            }
        }
    }
}