package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GreedyBotPlayerTest {

    private lateinit var bot: GreedyBotPlayer
    private lateinit var turn: Turn
    private lateinit var map: WritableMap
    private lateinit var cursor: PlayerCursor
    private lateinit var control: PlayerTurnControl
    private lateinit var moveDirections: MutableList<MoveDirection>
    private var numberOfStalemateMoves: Int = 0

    @Before
    fun setUp() {
        bot = GreedyBotPlayer(color = Color.RED)
        turn = Turn(1)

        val secondPlayer = NoOpPlayer(Color.GREEN)
        val thirdPlayer = NoOpPlayer(Color.BLUE)
        val initialCells = MapCellFactory.threeByFourCellsForThreePlayers(bot, secondPlayer, thirdPlayer)
        map = WritableMap(initialCells)

        val writableCursor = WritablePlayerCursor(bot, map)
        cursor = writableCursor
        moveDirections = mutableListOf()
        numberOfStalemateMoves = 0

        control = object: PlayerTurnControl {
            override val cursor: PlayerCursor
                get() = writableCursor

            override suspend fun makeMove(direction: MoveDirection) {
                moveDirections.add(direction)
                writableCursor.move(turn.next(), direction)
            }

            override suspend fun makeStalemateMove() {
                numberOfStalemateMoves++
            }
        }
    }

    @Test
    fun makeMove_shouldMoveRandomly_whenThereIsMovableDirections() = runTest {
        bot.makeMove(turn, control)

        val expectedMoveDirections = listOf(
            listOf(MoveDirection.DOWN),
            listOf(MoveDirection.RIGHT)
        )

        Assert.assertTrue(moveDirections.isNotEmpty())
        Assert.assertEquals(0, numberOfStalemateMoves)
        Assert.assertTrue(expectedMoveDirections.contains(moveDirections))
    }

    @Test
    fun makeMove_shouldNotMove_whenThereIsNoMovableDirections() = runTest {
        val cellsNotOccupiedByBot = map.cells.filter { it.occupier != bot }
        cellsNotOccupiedByBot.forEach { cell ->
            map.changeCell(cell.copy(occupier = bot))
        }

        bot.makeMove(turn, control)

        Assert.assertTrue(moveDirections.isEmpty())
        Assert.assertEquals(1, numberOfStalemateMoves)
    }

    @Test
    fun makeMove_shouldMoveTowardsPrizeToken_whenThereIsMovableDirections() = runTest {
        bot.makeMove(turn, control)
        bot.makeMove(turn, control)

        val expectedMoveDirections = listOf(
            listOf(MoveDirection.DOWN, MoveDirection.RIGHT),
            listOf(MoveDirection.RIGHT, MoveDirection.DOWN)
        )

        Assert.assertEquals(2, moveDirections.size)
        Assert.assertEquals(0, numberOfStalemateMoves)
        Assert.assertTrue(expectedMoveDirections.contains(moveDirections))
    }

    @Test
    fun makeMove_shouldNotMove_whenPrizeTokenIsAlreadyReached() = runTest {
        bot.makeMove(turn, control)
        bot.makeMove(turn, control)
        bot.makeMove(turn, control)
        bot.makeMove(turn, control)

        val expectedMoveDirections = listOf(
            listOf(MoveDirection.DOWN, MoveDirection.RIGHT),
            listOf(MoveDirection.RIGHT, MoveDirection.DOWN)
        )

        Assert.assertEquals(2, moveDirections.size)
        Assert.assertEquals(2, numberOfStalemateMoves)
        Assert.assertTrue(expectedMoveDirections.contains(moveDirections))
    }
}