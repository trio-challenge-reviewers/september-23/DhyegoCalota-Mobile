package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GameEndTurnClockObserverTest {

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    private lateinit var map: WritableMap

    @Before
    fun setup() {
        firstPlayer = NoOpPlayer(Color.RED)
        secondPlayer = NoOpPlayer(Color.GREEN)
        thirdPlayer = NoOpPlayer(Color.BLUE)

        val cells = MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer)
        map = WritableMap(cells)
    }

    @Test
    fun constructor_shouldReturnNullWinner() {
        val onWinner: GameWinnerObserver = {}
        val observer = GameEndTurnClockObserver(map, onWinner)

        Assert.assertNull(observer.winner)
        Assert.assertFalse(observer.isGameOver)
    }

    @Test
    fun invoke_shouldSetWinner_whenPrizeTokenIsOccupied() = runTest {
        val arbitraryCell = map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = secondPlayer)
        map.changeCell(arbitraryPrizeTokenCell)

        val onWinner: GameWinnerObserver = {}
        val observer = GameEndTurnClockObserver(map, onWinner)

        observer.invoke(Turn(0))

        Assert.assertEquals(secondPlayer, observer.winner)
        Assert.assertTrue(observer.isGameOver)
    }

    @Test
    fun invoke_shouldNotSetWinner_whenPrizeTokenIsNotOccupied() = runTest {
        val arbitraryCell = map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN)
        map.changeCell(arbitraryPrizeTokenCell)

        val onWinner: GameWinnerObserver = {}
        val observer = GameEndTurnClockObserver(map, onWinner)

        observer.invoke(Turn(0))

        Assert.assertNull(observer.winner)
        Assert.assertFalse(observer.isGameOver)
    }
}