package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class CoordinateDistanceCalculatorTest {

    @Test
    fun getDistance_shouldReturnDistanceBetweenTwoCoordinates() {
        val input = listOf(
            Triple(Coordinate(0, 0), Coordinate(0, 0), 0),
            Triple(Coordinate(0, 0), Coordinate(0, 1), 1),
            Triple(Coordinate(0, 0), Coordinate(1, 0), 1),
            Triple(Coordinate(0, 0), Coordinate(1, 1), 2),
            Triple(Coordinate(0, 0), Coordinate(2, 2), 4),
            Triple(Coordinate(0, 0), Coordinate(3, 3), 6),
            Triple(Coordinate(0, 0), Coordinate(4, 4), 8),
            Triple(Coordinate(0, 0), Coordinate(5, 5) ,10),
            Triple(Coordinate(3, 1), Coordinate(5, 5), 6),
            Triple(Coordinate(3, 1), Coordinate(5, 1), 2),
            Triple(Coordinate(3, 1), Coordinate(1, 5), 6),
            Triple(Coordinate(3, 1), Coordinate(1, 1), 2),
            Triple(Coordinate(3, 1), Coordinate(3, 1), 0),
            Triple(Coordinate(3, 1), Coordinate(3, 2), 1),
            Triple(Coordinate(8, 4), Coordinate(8, 4), 0),
            Triple(Coordinate(8, 4), Coordinate(8, 5), 1),
            Triple(Coordinate(8, 4), Coordinate(9, 4), 1),
            Triple(Coordinate(8, 4), Coordinate(9, 5), 2),
            Triple(Coordinate(8, 4), Coordinate(10, 6), 4),
            Triple(Coordinate(8, 4), Coordinate(11, 7), 6),
            Triple(Coordinate(8, 4), Coordinate(12, 8), 8),
            Triple(Coordinate(8, 4), Coordinate(13, 9) ,10),
        )

        input.forEach { (origin, destination, expectedDistance) ->
            val distance = CoordinateDistanceCalculator.getDistance(origin, destination)
            Assert.assertEquals(expectedDistance, distance)
        }
    }

    @Test
    fun getFarthestCoordinateWithinBoundaries_shouldReturnFarthestCoordinate_whenBoundariesAreSquare() {
        val boundaries = Boundaries(8, 8)
        val input = listOf(
            Triple(Coordinate(0, 0), setOf(Coordinate(7, 6)), Coordinate(7, 7)),
            Triple(Coordinate(0, 0), setOf(Coordinate(7, 7)), Coordinate(6, 7)),
            Triple(Coordinate(1, 5), setOf(Coordinate(7, 1)), Coordinate(7, 0)),
            Triple(Coordinate(1, 5), setOf(Coordinate(7, 0)), Coordinate(6, 0)),
            Triple(Coordinate(4, 3), setOf(Coordinate(0, 6)), Coordinate(0, 7)),
            Triple(Coordinate(4, 3), setOf(Coordinate(0, 7)), Coordinate(0, 0)),
            Triple(Coordinate(6, 1), setOf(Coordinate(0, 6)), Coordinate(0, 7)),
            Triple(Coordinate(6, 1), setOf(Coordinate(0, 7)), Coordinate(0, 6)),
            Triple(Coordinate(7, 6), setOf(Coordinate(0, 1)), Coordinate(0, 0)),
            Triple(Coordinate(7, 6), setOf(Coordinate(0, 0)), Coordinate(0, 1)),
            Triple(Coordinate(7, 7), setOf(Coordinate(0, 1)), Coordinate(0, 0)),
            Triple(Coordinate(7, 7), setOf(Coordinate(0, 0)), Coordinate(0, 1)),
        )

        input.forEach { (origin, excluded, expectedCoordinate) ->
            val coordinates = CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
                origins = setOf(origin),
                boundaries,
                excluded,
            )

            Assert.assertEquals(expectedCoordinate, coordinates.first())
        }
    }

    @Test
    fun getFarthestCoordinateWithinBoundaries_shouldThrowException_whenOriginIsNotWithinBoundaries() {
        val boundaries = Boundaries(8, 8)
        val input = listOf(
            Coordinate(8, 0),
            Coordinate(0, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
            Coordinate(8, 8),
        )

        input.forEach { origin ->
            val coordinates = kotlin.runCatching {
                CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
                    origins = setOf(origin),
                    boundaries = boundaries,
                    excluded = null,
                )
            }

            Assert.assertThrows(IllegalArgumentException::class.java) { coordinates.getOrThrow() }
        }
    }

    @Test
    fun getFarthestCoordinateWithinBoundaries_shouldThrowException_whenExcludedIsNotWithinBoundaries() {
        val boundaries = Boundaries(8, 8)
        val input = listOf(
            setOf(Coordinate(8, 0)),
            setOf(Coordinate(0, 8)),
            setOf(Coordinate(8, 8)),
            setOf(Coordinate(9, 8)),
            setOf(Coordinate(8, 9)),
        )

        input.forEach { excluded ->
            val coordinates = kotlin.runCatching {
                val origin = Coordinate(0, 0)
                CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
                    origins = setOf(origin),
                    boundaries = boundaries,
                    excluded = excluded,
                )
            }

            Assert.assertThrows(IllegalArgumentException::class.java) { coordinates.getOrThrow() }
        }
    }

    @Test
    fun getFarthestCoordinateWithinBoundaries_shouldReturnFarthestCoordinate_whenBoundariesAreRectangle() {
        val boundaries = Boundaries(8, 4)
        val input = listOf(
            Triple(Coordinate(0, 0), setOf(Coordinate(7, 2)), Coordinate(7, 3)),
            Triple(Coordinate(0, 0), setOf(Coordinate(7, 3)), Coordinate(6, 3)),
            Triple(Coordinate(1, 3), setOf(Coordinate(7, 1)), Coordinate(7, 0)),
            Triple(Coordinate(1, 3), setOf(Coordinate(7, 0)), Coordinate(6, 0)),
            Triple(Coordinate(4, 3), setOf(Coordinate(0, 1)), Coordinate(0, 0)),
            Triple(Coordinate(4, 3), setOf(Coordinate(0, 0)), Coordinate(0, 1)),
            Triple(Coordinate(6, 1), setOf(Coordinate(0, 2)), Coordinate(0, 3)),
            Triple(Coordinate(6, 1), setOf(Coordinate(0, 3)), Coordinate(0, 0)),
            Triple(Coordinate(7, 3), setOf(Coordinate(0, 1)), Coordinate(0, 0)),
            Triple(Coordinate(7, 3), setOf(Coordinate(0, 0)), Coordinate(0, 1)),
        )

        input.forEach { (origin, excluded, expectedCoordinate) ->
            val coordinates = CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
                origins = setOf(origin),
                boundaries = boundaries,
                excluded = excluded,
            )

            Assert.assertEquals(expectedCoordinate, coordinates.first())
        }
    }

    @Test
    fun getFarthestCoordinateWithinBoundaries_shouldReturnCommonFarthestCoordinateAmongAllOrigins_whenOtherOriginsAreDefined() {
        val boundaries = Boundaries(4, 5)
        val input = listOf(
            Triple(
                listOf(Coordinate(1, 1), Coordinate(2, 4)),
                setOf<Coordinate>(),
                Coordinate(3, 0)
            ),
            Triple(
                listOf(Coordinate(3, 0), Coordinate(0, 0)),
                setOf(Coordinate(3, 0)),
                Coordinate(2, 4)
            ),
        )

        input.forEach { (origins, excluded, expectedCoordinate) ->
            val (origin, otherOrigins) = origins.first() to origins.drop(1).toSet()
            val coordinates = CoordinateDistanceCalculator.getFarthestCoordinateWithinBoundaries(
                origins = setOf(origin).plus(otherOrigins),
                boundaries,
                excluded,
            )

            Assert.assertEquals(expectedCoordinate, coordinates.first())
        }
    }
}