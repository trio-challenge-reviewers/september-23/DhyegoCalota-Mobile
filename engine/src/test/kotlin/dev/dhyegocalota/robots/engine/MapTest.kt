package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MapTest {

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    private lateinit var oneByTwoCellsForOnePlayers: Set<MapCell>
    private lateinit var twoByTwoCellsForTwoPlayers: Set<MapCell>
    private lateinit var threeByFourCellsForThreePlayers: Set<MapCell>

    private lateinit var onePlayers: Set<Player>
    private lateinit var twoPlayers: Set<Player>
    private lateinit var threePlayers: Set<Player>

    @Before
    fun setUp() {
        firstPlayer = NoOpPlayer(color = Color.RED)
        secondPlayer = NoOpPlayer(color = Color.GREEN)
        thirdPlayer = NoOpPlayer(color = Color.BLUE)

        oneByTwoCellsForOnePlayers = MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer)
        twoByTwoCellsForTwoPlayers = MapCellFactory.twoByTwoCellsForTwoPlayers(firstPlayer, secondPlayer)
        threeByFourCellsForThreePlayers = MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer)

        onePlayers = setOf(firstPlayer)
        twoPlayers = setOf(firstPlayer, secondPlayer)
        threePlayers = setOf(firstPlayer, secondPlayer, thirdPlayer)
    }
}