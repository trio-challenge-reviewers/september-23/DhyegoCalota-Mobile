package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class CellReplacerMapTransformerTest {

    @Test
    fun transform_shouldReplaceCell() {
        val cellToReplace = MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND)
        val transformer = CellReplacerMapTransformer(cellToReplace)
        val initialCells = sortedSetOf(
            MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 1), type = MapCellType.LAND)
        )

        val transformedCells = transformer.transform(initialCells)
        val targetCell = transformedCells.first { it.coordinate == cellToReplace.coordinate }

        Assert.assertEquals(4, transformedCells.size)
        Assert.assertEquals(cellToReplace, targetCell)
    }

    @Test
    fun transform_shouldNotReplaceCell_whenCellDoesNotExist() {
        val cellToReplace = MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND)
        val transformer = CellReplacerMapTransformer(cellToReplace)
        val initialCells = sortedSetOf(
            MapCell(coordinate = Coordinate(0, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 1), type = MapCellType.LAND)
        )

        val transformedCells = kotlin.runCatching { transformer.transform(initialCells) }

        Assert.assertThrows(NoSuchElementException::class.java) {
            transformedCells.getOrThrow()
        }
    }
}