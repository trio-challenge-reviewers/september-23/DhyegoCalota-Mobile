package dev.dhyegocalota.robots.engine.factory

import dev.dhyegocalota.robots.engine.Coordinate
import dev.dhyegocalota.robots.engine.MapCell
import dev.dhyegocalota.robots.engine.MapCellType
import dev.dhyegocalota.robots.engine.Player

object MapCellFactory {

    fun oneByTwoCellsForOnePlayers(firstPlayer: Player) = setOf(
        MapCell(MapCellType.LAND, Coordinate(0, 0), firstPlayer),
        MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 0))
    )

    fun twoByTwoCellsForTwoPlayers(firstPlayer: Player, secondPlayer: Player) = setOf(
        MapCell(MapCellType.LAND, Coordinate(0, 0), firstPlayer),
        MapCell(MapCellType.LAND, Coordinate(0, 1)),
        MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 0)),
        MapCell(MapCellType.LAND, Coordinate(1, 1), secondPlayer),
    )

    fun threeByFourCellsForThreePlayers(firstPlayer: Player, secondPlayer: Player, thirdPlayer: Player) = setOf(
        MapCell(MapCellType.LAND, Coordinate(0, 0), firstPlayer),
        MapCell(MapCellType.LAND, Coordinate(0, 1)),
        MapCell(MapCellType.LAND, Coordinate(0, 2)),
        MapCell(MapCellType.LAND, Coordinate(0, 3)),
        MapCell(MapCellType.LAND, Coordinate(1, 0)),
        MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 1)),
        MapCell(MapCellType.LAND, Coordinate(1, 2), secondPlayer),
        MapCell(MapCellType.LAND, Coordinate(1, 3)),
        MapCell(MapCellType.LAND, Coordinate(2, 0)),
        MapCell(MapCellType.LAND, Coordinate(2, 1)),
        MapCell(MapCellType.LAND, Coordinate(2, 2)),
        MapCell(MapCellType.LAND, Coordinate(2, 3), thirdPlayer)
    )
}