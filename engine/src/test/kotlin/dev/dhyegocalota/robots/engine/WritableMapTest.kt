package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class WritableMapTest {

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    private lateinit var oneByTwoCellsForOnePlayers: Set<MapCell>
    private lateinit var twoByTwoCellsForTwoPlayers: Set<MapCell>
    private lateinit var threeByFourCellsForThreePlayers: Set<MapCell>

    @Before
    fun setUp() {
        firstPlayer = NoOpPlayer(color = Color.RED)
        secondPlayer = NoOpPlayer(color = Color.GREEN)
        thirdPlayer = NoOpPlayer(color = Color.BLUE)

        oneByTwoCellsForOnePlayers = MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer)
        twoByTwoCellsForTwoPlayers = MapCellFactory.twoByTwoCellsForTwoPlayers(firstPlayer, secondPlayer)
        threeByFourCellsForThreePlayers = MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer)
    }

    @Test
    fun constructor_shouldThrowException_whenCellsIsEmpty() {
        val map = kotlin.runCatching { WritableMap(initialCells = emptySet()) }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            map.getOrThrow()
        }
    }

    @Test
    fun constructor_shouldThrowException_whenMapDoesNotHaveAllCells() {
        val map = kotlin.runCatching {
            val mapWithoutOccupiedCell =
                threeByFourCellsForThreePlayers.map {
                    val invalidCoordinate =
                        it.coordinate.copy(x = it.coordinate.x + 1, y = it.coordinate.y)
                    it.copy(coordinate = invalidCoordinate)
                }

            WritableMap(initialCells = mapWithoutOccupiedCell.toSet())
        }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            map.getOrThrow()
        }
    }

    @Test
    fun constructor_shouldReturnMapWithCells() {
        val map = WritableMap(
            initialCells = twoByTwoCellsForTwoPlayers
        )

        Assert.assertEquals(twoByTwoCellsForTwoPlayers, map.cells)
    }

    @Test
    fun boundaries_shouldReturnBoundaries() {
        val maps = listOf(
            WritableMap(initialCells = oneByTwoCellsForOnePlayers) to Boundaries(2, 1),
            WritableMap(initialCells = twoByTwoCellsForTwoPlayers) to Boundaries(2, 2),
            WritableMap(initialCells = threeByFourCellsForThreePlayers) to Boundaries(3, 4),
        )

        maps.forEach { (map, expectedBoundaries) ->
            Assert.assertEquals(expectedBoundaries, map.boundaries)
        }
    }

    @Test
    fun coordinates_shouldReturnCoordinates() {
        val maps = listOf(
            WritableMap(initialCells = oneByTwoCellsForOnePlayers) to setOf(
                Coordinate(0, 0),
                Coordinate(1, 0),
            ),
            WritableMap(initialCells = twoByTwoCellsForTwoPlayers) to setOf(
                Coordinate(0, 0),
                Coordinate(1, 0),
                Coordinate(0, 1),
                Coordinate(1, 1),
            ),
            WritableMap(initialCells = threeByFourCellsForThreePlayers) to setOf(
                Coordinate(0, 0),
                Coordinate(1, 0),
                Coordinate(2, 0),
                Coordinate(0, 1),
                Coordinate(1, 1),
                Coordinate(2, 1),
                Coordinate(0, 2),
                Coordinate(1, 2),
                Coordinate(2, 2),
                Coordinate(0, 3),
                Coordinate(1, 3),
                Coordinate(2, 3),
            ),
        )

        maps.forEach { (map, expectedCoordinates) ->
            Assert.assertEquals(expectedCoordinates, map.coordinates)
        }
    }

    @Test
    fun getCell_shouldReturnCell() {
        val map = WritableMap(initialCells = threeByFourCellsForThreePlayers)

        val cells = listOf(
            Coordinate(0, 0) to MapCell(MapCellType.LAND, Coordinate(0, 0)),
            Coordinate(0, 1) to MapCell(MapCellType.LAND, Coordinate(0, 1)),
            Coordinate(0, 2) to MapCell(MapCellType.LAND, Coordinate(0, 2)),
            Coordinate(0, 3) to MapCell(MapCellType.LAND, Coordinate(0, 3)),
            Coordinate(1, 0) to MapCell(MapCellType.LAND, Coordinate(1, 0)),
            Coordinate(1, 1) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 1)),
            Coordinate(1, 2) to MapCell(MapCellType.LAND, Coordinate(1, 2)),
            Coordinate(1, 3) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            Coordinate(2, 0) to MapCell(MapCellType.LAND, Coordinate(2, 0)),
            Coordinate(2, 1) to MapCell(MapCellType.LAND, Coordinate(2, 1)),
            Coordinate(2, 2) to MapCell(MapCellType.LAND, Coordinate(2, 2)),
            Coordinate(2, 3) to MapCell(MapCellType.LAND, Coordinate(2, 3)),
        )

        cells.forEach { (coordinate, expectedCell) ->
            val cell = map.getCell(coordinate)

            Assert.assertTrue(expectedCell.coordinate == cell.coordinate)
        }
    }

    @Test
    fun changeCell_shouldChangeCell() {
        val map = WritableMap(initialCells = threeByFourCellsForThreePlayers)
        val cellToChange = MapCell(MapCellType.PRIZE_TOKEN, Coordinate(2, 2), firstPlayer)

        map.changeCell(cellToChange)

        val actualCell = map.getCell(Coordinate(2, 2))

        Assert.assertTrue(actualCell.coordinate == cellToChange.coordinate)
        Assert.assertTrue(actualCell.isPrizeToken())
        Assert.assertTrue(actualCell.isOccupied())
        Assert.assertTrue(actualCell.isOccupiedBy(firstPlayer))
    }

    @Test
    fun changeCell_shouldChangeCell_whenCellDoesNotExist() {
        val map = WritableMap(initialCells = threeByFourCellsForThreePlayers)
        val cellToChange = MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 4))

        val exception = kotlin.runCatching { map.changeCell(cellToChange) }

        Assert.assertThrows(NoSuchElementException::class.java) {
            exception.getOrThrow()
        }
    }

    @Test
    fun movePlayer_shouldMovePlayer() {
        val map = WritableMap(initialCells = threeByFourCellsForThreePlayers)
        val firstPlayerCursor = WritablePlayerCursor(firstPlayer, map)
        val initialCoordinate = firstPlayerCursor.initialCoordinate
        val expectedCoordinate = Coordinate(initialCoordinate.x + 1, initialCoordinate.y)

        map.movePlayer(Turn(1), firstPlayerCursor, MoveDirection.RIGHT)

        val actualCoordinate = firstPlayerCursor.currentCoordinate

        Assert.assertEquals(expectedCoordinate, actualCoordinate)
        Assert.assertEquals(listOf(initialCoordinate, expectedCoordinate), firstPlayerCursor.turnHistory.coordinates)
    }

    @Test
    fun movePlayer_shouldThrowException_whenMoveIsNotAllowed() {
        val map = WritableMap(initialCells = threeByFourCellsForThreePlayers)
        val firstPlayerCursor = WritablePlayerCursor(firstPlayer, map)

        val exception = kotlin.runCatching { map.movePlayer(Turn(1), firstPlayerCursor, MoveDirection.LEFT) }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            exception.getOrThrow()
        }
    }
}