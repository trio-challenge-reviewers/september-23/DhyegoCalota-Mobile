package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class CoordinateTest {

    @Test
    fun build_shouldReturnCoordinate_whenCoordinateIsValid() {
        val coordinatePairs = listOf(
            "0,0" to Coordinate(0, 0),
            "1,1" to Coordinate(1, 1),
            "5,5" to Coordinate(5, 5)
        )

        coordinatePairs.forEach { (input, expectedOutput) ->
            val coordinate = Coordinate.build(input)
            Assert.assertEquals(expectedOutput, coordinate)
        }
    }

    @Test
    fun build_shouldThrowException_whenCoordinateIsInvalid() {
        val coordinates = listOf(
            kotlin.runCatching { Coordinate.build("-1,-1") },
            kotlin.runCatching { Coordinate.build("0,-1") },
            kotlin.runCatching { Coordinate.build(",0") },
            kotlin.runCatching { Coordinate.build("0,") },
            kotlin.runCatching { Coordinate.build("0") },
            kotlin.runCatching { Coordinate.build("0,0,0") },
            kotlin.runCatching { Coordinate.build("") },
            kotlin.runCatching { Coordinate.build("xy") },
            kotlin.runCatching { Coordinate.build(" 1, 2") },
            kotlin.runCatching { Coordinate.build("1, 2 ") },
            kotlin.runCatching { Coordinate.build(" 1,2") }
        )

        coordinates.forEach { coordinate ->
            Assert.assertThrows(IllegalArgumentException::class.java) { coordinate.getOrThrow() }
        }
    }

    @Test
    fun constructor_shouldThrowException_whenCoordinateIsInvalid() {
        val coordinate = kotlin.runCatching { Coordinate(-1, -1) }

        Assert.assertThrows(IllegalArgumentException::class.java) { coordinate.getOrThrow() }
    }

    @Test
    fun constructor_shouldNotThrowException_whenCoordinateIsValid() {
        val coordinate = Coordinate(0, 0)

        Assert.assertEquals(Coordinate(0, 0), coordinate)
    }

    @Test
    fun equals_shouldReturnTrue_whenCoordinatesAreTheSame() {
        val coordinate = Coordinate(0, 0)
        val otherCoordinate = Coordinate(0, 0)

        Assert.assertTrue(coordinate == otherCoordinate)
    }

    @Test
    fun equals_shouldReturnFalse_whenCoordinatesAreNotTheSame() {
        val coordinate = Coordinate(0, 0)
        val otherCoordinate = Coordinate(1, 1)

        Assert.assertFalse(coordinate == otherCoordinate)
    }

    @Test
    fun equals_shouldReturnFalse_whenOtherObjectIsNotACoordinate() {
        val coordinate = Coordinate(0, 0)
        val otherObject = Any()

        Assert.assertFalse(coordinate == otherObject)
    }

    @Test
    fun equals_shouldReturnFalse_whenOtherObjectIsNull() {
        val coordinate = Coordinate(0, 0)
        val otherObject = null

        Assert.assertFalse(coordinate == otherObject)
    }

    @Test
    fun hashCode_shouldReturnSameValue_whenCoordinatesAreTheSame() {
        val coordinate = Coordinate(0, 0)
        val otherCoordinate = Coordinate(0, 0)

        Assert.assertEquals(coordinate.hashCode(), otherCoordinate.hashCode())
    }

    @Test
    fun hashCode_shouldReturnDifferentValue_whenCoordinatesAreNotTheSame() {
        val coordinate = Coordinate(0, 0)
        val otherCoordinate = Coordinate(1, 1)

        Assert.assertNotEquals(coordinate.hashCode(), otherCoordinate.hashCode())
    }

    @Test
    fun compareTo_shouldReturnZero_whenCoordinatesAreTheSame() {
        val coordinates = listOf(
            Coordinate(0, 0) to Coordinate(0, 0),
            Coordinate(1, 1) to Coordinate(1, 1),
            Coordinate(5, 5) to Coordinate(5, 5)
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertEquals(0, leftCoordinate.compareTo(rightCoordinate))
        }
    }

    @Test
    fun compareTo_shouldReturnNegativeValue_whenLeftCoordinateIsLessThanRightCoordinate() {
        val coordinates = listOf(
            Coordinate(0, 0) to Coordinate(0, 1),
            Coordinate(1, 1) to Coordinate(1, 2),
            Coordinate(5, 5) to Coordinate(5, 6),
            Coordinate(5, 5) to Coordinate(6, 5),
            Coordinate(5, 5) to Coordinate(6, 6),
            Coordinate(5, 5) to Coordinate(6, 7),
            Coordinate(5, 5) to Coordinate(7, 5),
            Coordinate(5, 5) to Coordinate(7, 6),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) < 0)
        }
    }

    @Test
    fun compareTo_shouldReturnPositiveValue_whenLeftCoordinateIsGreaterThanRightCoordinate() {
        val coordinates = listOf(
            Coordinate(0, 1) to Coordinate(0, 0),
            Coordinate(1, 2) to Coordinate(1, 1),
            Coordinate(5, 6) to Coordinate(5, 5),
            Coordinate(6, 5) to Coordinate(5, 5),
            Coordinate(6, 6) to Coordinate(5, 5),
            Coordinate(6, 7) to Coordinate(5, 5),
            Coordinate(7, 5) to Coordinate(5, 5),
            Coordinate(7, 6) to Coordinate(5, 5),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) > 0)
        }
    }

    @Test
    fun compareTo_shouldReturnNegativeValue_whenLeftCoordinateIsLessThanRightCoordinateByX() {
        val coordinates = listOf(
            Coordinate(0, 0) to Coordinate(1, 0),
            Coordinate(1, 1) to Coordinate(2, 1),
            Coordinate(5, 5) to Coordinate(6, 5),
            Coordinate(5, 5) to Coordinate(6, 6),
            Coordinate(5, 5) to Coordinate(6, 7),
            Coordinate(5, 5) to Coordinate(7, 5),
            Coordinate(5, 5) to Coordinate(7, 6),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) < 0)
        }
    }

    @Test
    fun compareTo_shouldReturnPositiveValue_whenLeftCoordinateIsGreaterThanRightCoordinateByX() {
        val coordinates = listOf(
            Coordinate(1, 0) to Coordinate(0, 0),
            Coordinate(2, 1) to Coordinate(1, 1),
            Coordinate(6, 5) to Coordinate(5, 5),
            Coordinate(6, 6) to Coordinate(5, 5),
            Coordinate(6, 7) to Coordinate(5, 5),
            Coordinate(7, 5) to Coordinate(5, 5),
            Coordinate(7, 6) to Coordinate(5, 5),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) > 0)
        }
    }

    @Test
    fun compareTo_shouldReturnNegativeValue_whenLeftCoordinateIsLessThanRightCoordinateByY() {
        val coordinates = listOf(
            Coordinate(0, 0) to Coordinate(0, 1),
            Coordinate(1, 1) to Coordinate(1, 2),
            Coordinate(5, 5) to Coordinate(5, 6),
            Coordinate(5, 5) to Coordinate(6, 6),
            Coordinate(5, 5) to Coordinate(6, 7),
            Coordinate(5, 5) to Coordinate(7, 7),
            Coordinate(5, 5) to Coordinate(7, 8),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) < 0)
        }
    }

    @Test
    fun compareTo_shouldReturnPositiveValue_whenLeftCoordinateIsGreaterThanRightCoordinateByY() {
        val coordinates = listOf(
            Coordinate(0, 1) to Coordinate(0, 0),
            Coordinate(1, 2) to Coordinate(1, 1),
            Coordinate(5, 6) to Coordinate(5, 5),
            Coordinate(6, 6) to Coordinate(5, 5),
            Coordinate(6, 7) to Coordinate(5, 5),
            Coordinate(7, 7) to Coordinate(5, 5),
            Coordinate(7, 8) to Coordinate(5, 5),
        )

        coordinates.forEach { (leftCoordinate, rightCoordinate) ->
            Assert.assertTrue(leftCoordinate.compareTo(rightCoordinate) > 0)
        }
    }

    @Test
    fun toString_shouldReturnStringRepresentationOfCoordinate() {
        val coordinates = listOf(
            Coordinate(0, 0) to "0,0",
            Coordinate(1, 1) to "1,1",
            Coordinate(5, 5) to "5,5"
        )

        coordinates.forEach { (coordinate, expectedOutput) ->
            Assert.assertEquals(expectedOutput, coordinate.toString())
        }
    }
}