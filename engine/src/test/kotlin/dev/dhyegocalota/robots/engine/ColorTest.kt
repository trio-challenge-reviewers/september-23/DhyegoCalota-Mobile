package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class ColorTest {

    @Test
    fun equals_shouldReturnTrue_whenComparingSameInstance() {
        val color = Color.PURPLE

        Assert.assertTrue(color == color)
    }

    @Test
    fun equals_shouldReturnTrue_whenComparingDifferentInstancesWithSameValues() {
        val color1 = Color.PURPLE
        val color2 = Color.PURPLE

        Assert.assertTrue(color1 == color2)
    }

    @Test
    fun equals_shouldReturnFalse_whenComparingDifferentInstancesWithDifferentValues() {
        val color1 = Color.PURPLE
        val color2 = Color.GREEN

        Assert.assertFalse(color1.equals(color2))
    }

    @Test
    fun hashCode_shouldReturnSameValue_whenComparingSameInstance() {
        val color = Color.PURPLE

        Assert.assertEquals(color.hashCode(), color.hashCode())
    }

    @Test
    fun hashCode_shouldReturnSameValue_whenComparingDifferentInstancesWithSameValues() {
        val color1 = Color.PURPLE
        val color2 = Color.PURPLE

        Assert.assertEquals(color1.hashCode(), color2.hashCode())
    }

    @Test
    fun hashCode_shouldReturnDifferentValue_whenComparingDifferentInstancesWithDifferentValues() {
        val color1 = Color.PURPLE
        val color2 = Color.GREEN

        Assert.assertNotEquals(color1.hashCode(), color2.hashCode())
    }

    @Test
    fun equals_shouldReturnFalse_whenComparingWithNull() {
        val color = Color.PURPLE

        Assert.assertFalse(color == null)
    }

    @Test
    fun equals_shouldReturnFalse_whenComparingWithDifferentClass() {
        val color = Color.PURPLE

        Assert.assertFalse(color == Any())
    }

    @Test
    fun isReserved_shouldReturnTrue_whenColorIsReserved() {
        val color = Color.YELLOW

        Assert.assertTrue(color.isReserved)
    }

    @Test
    fun isReserved_shouldReturnFalse_whenColorIsNotReserved() {
        val color = Color.PURPLE

        Assert.assertFalse(color.isReserved)
    }

    @Test
    fun nonActiveHex_shouldReturnNonActiveHex() {
        val color = Color.PURPLE

        Assert.assertTrue(color.nonActiveHex.startsWith("#"))
    }

    @Test
    fun activeHex_shouldReturnActiveHex() {
        val color = Color.PURPLE

        Assert.assertTrue(color.nonActiveHex.startsWith("#"))
    }
}