package dev.dhyegocalota.robots.engine.config

import dev.dhyegocalota.robots.engine.Color
import dev.dhyegocalota.robots.engine.Player
import dev.dhyegocalota.robots.engine.PlayerCursor
import dev.dhyegocalota.robots.engine.PlayerTurnControl
import dev.dhyegocalota.robots.engine.Turn

class NoOpPlayer(override val color: Color) : Player(color) {
    val turns = mutableListOf<Turn>()
    private val moveControls = mutableListOf<Pair<Turn, PlayerTurnControl>>()

    val numberOfMoves: Int
        get() = moveControls.size

    override fun onTurn(turn: Turn, cursor: PlayerCursor) {
        turns.add(turn)
    }

    override suspend fun makeMove(turn: Turn, control: PlayerTurnControl) {
        moveControls.add(turn to control)
    }
}