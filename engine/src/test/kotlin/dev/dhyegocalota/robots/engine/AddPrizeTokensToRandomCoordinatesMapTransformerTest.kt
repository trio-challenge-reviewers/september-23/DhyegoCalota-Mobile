package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class AddPrizeTokensToRandomCoordinatesMapTransformerTest {

    @Test
    fun constructor_shouldThrowException_whenNumberOfPrizeTokensIsLessThanOne() {
        val transformer = kotlin.runCatching { AddPrizeTokensToRandomCoordinatesMapTransformer(0) }

        Assert.assertThrows(IllegalArgumentException::class.java) { transformer.getOrThrow() }
    }

    @Test
    fun transform_shouldThrowException_whenNumberOfPrizeTokensIsGreaterThanNumberOfUnoccupiedCells() {
        val cells = sortedSetOf(
            MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND, occupier = NoOpPlayer(color = Color.BLUE)),
        )

        val transformer = kotlin.runCatching { AddPrizeTokensToRandomCoordinatesMapTransformer(1).transform(cells) }

        Assert.assertThrows(IllegalArgumentException::class.java) { transformer.getOrThrow() }
    }

    @Test
    fun transform_shouldReturnCellsWithDefinedNumberOfPrizeTokens() {
        val cells = setOf(
            MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND, occupier = NoOpPlayer(color = Color.BLUE)),
            MapCell(coordinate = Coordinate(0, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 2), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 3), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 4), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 5), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 6), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 7), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 8), type = MapCellType.LAND, occupier = NoOpPlayer(color = Color.RED)),
            MapCell(coordinate = Coordinate(0, 9), type = MapCellType.LAND),
        )

        val setOfTransformedCells = setOf(
            AddPrizeTokensToRandomCoordinatesMapTransformer(1).transform(cells.toSortedSet()) to 1,
            AddPrizeTokensToRandomCoordinatesMapTransformer(2).transform(cells.toSortedSet()) to 2,
            AddPrizeTokensToRandomCoordinatesMapTransformer(3).transform(cells.toSortedSet()) to 3,
            AddPrizeTokensToRandomCoordinatesMapTransformer(4).transform(cells.toSortedSet()) to 4,
            AddPrizeTokensToRandomCoordinatesMapTransformer(5).transform(cells.toSortedSet()) to 5,
        )

        setOfTransformedCells.forEach { (transformedCells, numberOfPrizeTokens) ->
            val prizeTokens = transformedCells.filter { it.isPrizeToken() }
            Assert.assertEquals(numberOfPrizeTokens, prizeTokens.size)
        }
    }
}