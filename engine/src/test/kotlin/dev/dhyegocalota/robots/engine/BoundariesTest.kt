package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class BoundariesTest {

    @Test
    fun constructor_shouldThrowException_whenInvalidBoundaries() {
        val boundaries = listOf(
            -1 to 2,
            1 to -2,
            -1 to -2
        )

        boundaries.forEach { (width, height) ->
            val boundaries = kotlin.runCatching { Boundaries(width, height) }

            Assert.assertThrows(IllegalArgumentException::class.java) {
                boundaries.getOrThrow()
            }
        }
    }

    @Test
    fun constructor_shouldReturnBoundaries() {
        val boundaries = Boundaries(1, 2)

        Assert.assertEquals(1, boundaries.width)
        Assert.assertEquals(2, boundaries.height)
    }

    @Test
    fun build_shouldReturnBoundaries_whenValidBoundaries() {
        val boundaries = listOf(
            "1x2" to Boundaries(1, 2),
            "10x20" to Boundaries(10, 20),
            "100x200" to Boundaries(100, 200)
        )

        boundaries.forEach { (boundaries, expectedBoundaries) ->
            val boundaries = Boundaries.build(boundaries)

            Assert.assertEquals(expectedBoundaries, boundaries)
        }
    }

    @Test
    fun build_shouldThrowException_whenInvalidBoundaries() {
        val boundaries = listOf(
            "1x",
            "x1",
            "1x2x3",
            "1",
            "x",
            "x1x2",
            "1 x2",
            "1x 2",
            "1x2 ",
            " 1x2"
        )

        boundaries.forEach { boundaries ->
            val boundaries = kotlin.runCatching { Boundaries.build(boundaries) }

            Assert.assertThrows(IllegalArgumentException::class.java) {
                boundaries.getOrThrow()
            }
        }
    }

    @Test
    fun equals_shouldReturnTrue_whenSameBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to Boundaries(1, 2),
            Boundaries(10, 20) to Boundaries(10, 20),
            Boundaries(100, 200) to Boundaries(100, 200)
        )

        boundaries.forEach { (boundaries1, boundaries2) ->
            Assert.assertEquals(boundaries1, boundaries2)
        }
    }

    @Test
    fun equals_shouldReturnFalse_whenDifferentBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to Boundaries(1, 3),
            Boundaries(10, 20) to Boundaries(10, 21),
            Boundaries(100, 200) to Boundaries(101, 200)
        )

        boundaries.forEach { (boundaries1, boundaries2) ->
            Assert.assertNotEquals(boundaries1, boundaries2)
        }
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenSameBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to Boundaries(1, 2),
            Boundaries(10, 20) to Boundaries(10, 20),
            Boundaries(100, 200) to Boundaries(100, 200)
        )

        boundaries.forEach { (boundaries1, boundaries2) ->
            Assert.assertEquals(boundaries1.hashCode(), boundaries2.hashCode())
        }
    }

    @Test
    fun hashCode_shouldReturnDifferentHashCode_whenDifferentBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to Boundaries(1, 3),
            Boundaries(10, 20) to Boundaries(10, 21),
            Boundaries(100, 200) to Boundaries(101, 200)
        )

        boundaries.forEach { (boundaries1, boundaries2) ->
            Assert.assertNotEquals(boundaries1.hashCode(), boundaries2.hashCode())
        }
    }

    @Test
    fun toString_shouldReturnStringRepresentation() {
        val boundaries = listOf(
            Boundaries(1, 2) to "1x2",
            Boundaries(10, 20) to "10x20",
            Boundaries(100, 200) to "100x200"
        )

        boundaries.forEach { (boundaries, expectedStringRepresentation) ->
            Assert.assertEquals(expectedStringRepresentation, boundaries.toString())
        }
    }

    @Test
    fun countCells_shouldReturnNumberOfCells() {
        val boundaries = listOf(
            Boundaries(1, 2) to 2,
            Boundaries(10, 20) to 200,
            Boundaries(100, 200) to 20000
        )

        boundaries.forEach { (boundaries, expectedNumberOfCells) ->
            Assert.assertEquals(expectedNumberOfCells, boundaries.countCells())
        }
    }

    @Test
    fun buildCoordinates_shouldReturnAllSortedCoordinates() {
        val boundaries = listOf(
            Boundaries(1, 2) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1)
            ),
            Boundaries(2, 2) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1),
                Coordinate(1, 0),
                Coordinate(1, 1)
            ),
            Boundaries(2, 3) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1),
                Coordinate(0, 2),
                Coordinate(1, 0),
                Coordinate(1, 1),
                Coordinate(1, 2)
            )
        )

        boundaries.forEach { (boundaries, expectedCoordinates) ->
            val coordinates = boundaries.buildCoordinates()

            expectedCoordinates.forEachIndexed { index, expectedCoordinate ->
                Assert.assertEquals(expectedCoordinate, coordinates.elementAt(index))
            }
        }
    }

    @Test
    fun isWithin_shouldReturnTrue_whenCoordinateIsWithinBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1)
            ),
            Boundaries(2, 2) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1),
                Coordinate(1, 0),
                Coordinate(1, 1)
            ),
            Boundaries(2, 3) to setOf(
                Coordinate(0, 0),
                Coordinate(0, 1),
                Coordinate(0, 2),
                Coordinate(1, 0),
                Coordinate(1, 1),
                Coordinate(1, 2)
            )
        )

        boundaries.forEach { (boundaries, coordinates) ->
            coordinates.forEach { coordinate ->
                Assert.assertTrue(boundaries.isWithin(coordinate))
            }
        }
    }

    @Test
    fun isWithin_shouldReturnFalse_whenCoordinateIsNotWithinBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to setOf(
                Coordinate(1, 0),
                Coordinate(1, 1),
                Coordinate(0, 2),
                Coordinate(1, 2)
            ),
            Boundaries(2, 2) to setOf(
                Coordinate(2, 0),
                Coordinate(2, 1),
                Coordinate(0, 2),
                Coordinate(2, 2)
            ),
            Boundaries(2, 3) to setOf(
                Coordinate(2, 0),
                Coordinate(2, 1),
                Coordinate(2, 2),
                Coordinate(0, 3),
                Coordinate(2, 3)
            )
        )

        boundaries.forEach { (boundaries, coordinates) ->
            coordinates.forEach { coordinate ->
                Assert.assertFalse(boundaries.isWithin(coordinate))
            }
        }
    }

    @Test
    fun isMovable_shouldReturnFalse_whenCoordinateIsNotWithinBoundaries() {
        val boundaries = listOf(
            Boundaries(1, 2) to setOf(
                Coordinate(0, 0) to MoveDirection.LEFT,
                Coordinate(0, 1) to MoveDirection.RIGHT
            ),
            Boundaries(2, 2) to setOf(
                Coordinate(0, 0) to MoveDirection.LEFT,
                Coordinate(1, 1) to MoveDirection.RIGHT,
                Coordinate(1, 0) to MoveDirection.UP,
                Coordinate(1, 1) to MoveDirection.DOWN
            ),
            Boundaries(2, 3) to setOf(
                Coordinate(0, 0) to MoveDirection.LEFT,
                Coordinate(1, 2) to MoveDirection.RIGHT,
                Coordinate(1, 0) to MoveDirection.UP,
                Coordinate(1, 2) to MoveDirection.DOWN
            )
        )

        boundaries.forEach { (boundaries, coordinates) ->
            coordinates.forEach { (coordinate, direction) ->
                Assert.assertFalse(boundaries.isMovable(coordinate, direction))
            }
        }
    }

    @Test
    fun isMovable_shouldReturnTrue_whenCoordinateIsWithinBoundaries() {
        val boundaries = listOf(
            Boundaries(2, 2) to setOf(
                Coordinate(0, 0) to MoveDirection.RIGHT,
                Coordinate(1, 1) to MoveDirection.LEFT
            ),
            Boundaries(3, 3) to setOf(
                Coordinate(0, 0) to MoveDirection.RIGHT,
                Coordinate(1, 0) to MoveDirection.LEFT,
                Coordinate(1, 0) to MoveDirection.DOWN,
                Coordinate(1, 1) to MoveDirection.UP
            ),
            Boundaries(3, 3) to setOf(
                Coordinate(0, 0) to MoveDirection.RIGHT,
                Coordinate(1, 0) to MoveDirection.LEFT,
                Coordinate(1, 2) to MoveDirection.LEFT,
                Coordinate(1, 0) to MoveDirection.DOWN,
                Coordinate(1, 1) to MoveDirection.UP,
                Coordinate(1, 2) to MoveDirection.UP
            )
        )

        boundaries.forEach { (boundaries, coordinates) ->
            coordinates.forEach { (coordinate, direction) ->
                Assert.assertTrue(boundaries.isMovable(coordinate, direction))
            }
        }
    }
}