package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class TurnTest {

    @Test
    fun constructor_shouldSetNumberTo1_whenNoArgumentsArePassed() {
        val turn = Turn()

        Assert.assertEquals(1, turn.number)
    }

    @Test
    fun constructor_shouldSetNumberTo2_when2IsPassedAsArgument() {
        val turn = Turn(2)

        Assert.assertEquals(2, turn.number)
    }

    @Test
    fun isFirst_shouldReturnTrue_whenNumberIs1() {
        val turn = Turn()

        Assert.assertTrue(turn.isFirst())
    }

    @Test
    fun isFirst_shouldReturnFalse_whenNumberIsNot1() {
        val turns = listOf(2, 3, 4, 5)

        turns.forEach { turnNumber ->
            val turn = Turn(turnNumber)
            Assert.assertFalse(turn.isFirst())
        }
    }

    @Test
    fun next_shouldReturnTurnNextTurnNumber() {
        val turns = listOf(
            1 to 2,
            2 to 3,
            3 to 4,
            4 to 5,
        )

        turns.forEach { (currentTurnNumber, expectedNextTurnNumber) ->
            val turn = Turn(currentTurnNumber)

            Assert.assertEquals(expectedNextTurnNumber, turn.next().number)
        }
    }

    @Test
    fun equals_shouldReturnTrue_whenTurnsHaveSameNumber() {
        val turns = listOf(
            1 to 1,
            2 to 2,
            3 to 3,
            4 to 4,
        )

        turns.forEach { (turnNumber, otherTurnNumber) ->
            val turn = Turn(turnNumber)
            val otherTurn = Turn(otherTurnNumber)

            Assert.assertEquals(turn, otherTurn)
        }
    }

    @Test
    fun equals_shouldReturnFalse_whenTurnsHaveDifferentNumber() {
        val turns = listOf(
            1 to 2,
            2 to 3,
            3 to 4,
            4 to 5,
        )

        turns.forEach { (turnNumber, otherTurnNumber) ->
            val turn = Turn(turnNumber)
            val otherTurn = Turn(otherTurnNumber)

            Assert.assertNotEquals(turn, otherTurn)
        }
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenTurnsHaveSameNumber() {
        val turns = listOf(
            1 to 1,
            2 to 2,
            3 to 3,
            4 to 4,
        )

        turns.forEach { (turnNumber, otherTurnNumber) ->
            val turn = Turn(turnNumber)
            val otherTurn = Turn(otherTurnNumber)

            Assert.assertEquals(turn.hashCode(), otherTurn.hashCode())
        }
    }

    @Test
    fun hashCode_shouldReturnDifferentHashCode_whenTurnsHaveDifferentNumber() {
        val turns = listOf(
            1 to 2,
            2 to 3,
            3 to 4,
            4 to 5,
        )

        turns.forEach { (turnNumber, otherTurnNumber) ->
            val turn = Turn(turnNumber)
            val otherTurn = Turn(otherTurnNumber)

            Assert.assertNotEquals(turn.hashCode(), otherTurn.hashCode())
        }
    }

    @Test
    fun toString_shouldReturnTurnNumber() {
        val turns = listOf(
            1 to "1",
            2 to "2",
            3 to "3",
            4 to "4",
        )

        turns.forEach { (turnNumber, expectedToString) ->
            val turn = Turn(turnNumber)

            Assert.assertEquals(expectedToString, turn.toString())
        }
    }
}