package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class WritableTurnHistoryTest {

    @Test
    fun initialCoordinate_shouldReturnInitialCoordinate() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        Assert.assertEquals(initialCoordinate, writableTurnHistory.initialCoordinate)
    }

    @Test
    fun history_shouldReturnEmptySetWhenNoTurnWasAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        Assert.assertEquals(emptyList<Pair<Turn, Move>>(), writableTurnHistory.history)
    }

    @Test
    fun moves_shouldReturnEmptySetWhenNoTurnWasAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        Assert.assertEquals(emptyList<Move>(), writableTurnHistory.moves)
    }

    @Test
    fun coordinates_shouldReturnSetWithInitialCoordinateWhenNoTurnWasAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        Assert.assertEquals(listOf(initialCoordinate), writableTurnHistory.coordinates)
    }

    @Test
    fun add_shouldAddTurnAndMoveToHistory() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        writableTurnHistory.add(Turn(1), Move(initialCoordinate, MoveDirection.UP))
        writableTurnHistory.add(Turn(2), Move(initialCoordinate, MoveDirection.DOWN))

        Assert.assertEquals(
            listOf(
                Turn(1) to Move(initialCoordinate, MoveDirection.UP),
                Turn(2) to Move(initialCoordinate, MoveDirection.DOWN)
            ),
            writableTurnHistory.history
        )
    }

    @Test
    fun moves_shouldReturnSetWithAllMovesAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        writableTurnHistory.add(Turn(1), Move(initialCoordinate, MoveDirection.UP))
        writableTurnHistory.add(Turn(2), Move(initialCoordinate, MoveDirection.DOWN))

        Assert.assertEquals(
            listOf(
                Move(initialCoordinate, MoveDirection.UP),
                Move(initialCoordinate, MoveDirection.DOWN)
            ),
            writableTurnHistory.moves
        )
    }

    @Test
    fun coordinates_shouldReturnSetWithAllCoordinatesAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        writableTurnHistory.add(Turn(1), Move(initialCoordinate, MoveDirection.UP))
        writableTurnHistory.add(Turn(2), Move(Coordinate(1, 1), MoveDirection.RIGHT))

        Assert.assertEquals(
            listOf(
                initialCoordinate,
                Coordinate(1, 1),
                Coordinate(2, 1)
            ),
            writableTurnHistory.coordinates
        )
    }

    @Test
    fun currentCoordinate_shouldReturnLastCoordinateAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        writableTurnHistory.add(Turn(1), Move(initialCoordinate, MoveDirection.UP))
        writableTurnHistory.add(Turn(2), Move(Coordinate(1, 1), MoveDirection.RIGHT))

        Assert.assertEquals(
            Coordinate(2, 1),
            writableTurnHistory.currentCoordinate
        )
    }

    @Test
    fun currentCoordinate_shouldReturnInitialCoordinateWhenNoTurnWasAdded() {
        val initialCoordinate = Coordinate(1, 2)
        val writableTurnHistory = WritableTurnHistory(initialCoordinate)

        Assert.assertEquals(
            initialCoordinate,
            writableTurnHistory.currentCoordinate
        )
    }
}