package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GameEngineBuilderTest {

    private lateinit var logger: Logger

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    @Before
    fun setup() {
        logger = PrintLnLogger()

        firstPlayer = NoOpPlayer(Color.RED)
        secondPlayer = NoOpPlayer(Color.GREEN)
        thirdPlayer = NoOpPlayer(Color.BLUE)
    }

    @Test
    fun build_shouldThrowException_whenLoggerIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldThrowException_whenOnStartIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldThrowException_whenOnWinnerIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .withOnStart { }
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldThrowException_whenOnStopIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .withOnStart { }
                .withOnWinner { }
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldThrowException_whenOnTurnIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .withOnStart { }
                .withOnWinner { }
                .withOnStop { }
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldThrowException_whenBoundariesIsNotSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .withOnStart { }
                .withOnWinner { }
                .withOnStop { }
                .withOnTurn { }
                .build()
        }

        Assert.assertThrows(IllegalStateException::class.java) {
            engine.getOrThrow()
        }
    }

    @Test
    fun build_shouldReturnGameEngine_whenAllRequiredFieldsAreSet() {
        val engine = kotlin.runCatching {
            GameEngineBuilder()
                .withLogger(logger)
                .withOnStart { }
                .withOnWinner { }
                .withOnStop { }
                .withOnTurn { }
                .withBoundaries(Boundaries(10, 10))
                .withNumberOfPrizeTokens(10)
                .withPlayer(firstPlayer)
                .build()
        }

        Assert.assertTrue(engine.isSuccess)
        Assert.assertEquals(GameEngine::class.java, engine.getOrThrow()::class.java)
    }
}