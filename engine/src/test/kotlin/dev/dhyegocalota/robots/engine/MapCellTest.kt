package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class MapCellTest {
    @Test
    fun isEmpty_shouldReturnTrue_whenCellIsNotOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isEmpty())
        }
    }

    @Test
    fun isEmpty_shouldReturnFalse_whenCellIsOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isEmpty())
        }
    }

    @Test
    fun isPrizeToken_shouldReturnTrue_whenCellIsPrizeToken() {
        val cells = listOf(
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(0, 0)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 2)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isPrizeToken())
        }
    }

    @Test
    fun isPrizeToken_shouldReturnFalse_whenCellIsNotPrizeToken() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isPrizeToken())
        }
    }

    @Test
    fun isLand_shouldReturnTrue_whenCellIsLand() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isLand())
        }
    }

    @Test
    fun isLand_shouldReturnFalse_whenCellIsNotLand() {
        val cells = listOf(
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(0, 0)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 2)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isLand())
        }
    }

    @Test
    fun isOccupied_shouldReturnTrue_whenCellIsOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isOccupied())
        }
    }

    @Test
    fun isOccupied_shouldReturnFalse_whenCellIsNotOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isOccupied())
        }
    }

    @Test
    fun isOccupiedBy_shouldReturnTrue_whenCellIsOccupiedByPlayer() {
        val player = NoOpPlayer(color = Color.RED)
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), player),
            MapCell(MapCellType.LAND, Coordinate(1, 2), player),
            MapCell(MapCellType.LAND, Coordinate(3, 2), player),
            MapCell(MapCellType.LAND, Coordinate(1, 4), player)
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isOccupiedBy(player))
        }
    }

    @Test
    fun isOccupiedBy_shouldReturnFalse_whenCellIsNotOccupiedByPlayer() {
        val player = NoOpPlayer(color = Color.RED)
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isOccupiedBy(player))
        }
    }

    @Test
    fun canBeOccupied_shouldReturnTrue_whenCellIsNotOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.canBeOccupied())
        }
    }

    @Test
    fun canBeOccupied_shouldReturnFalse_whenCellIsOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.canBeOccupied())
        }
    }

    @Test
    fun occupy_shouldReturnNewCell_whenCellIsNotOccupied() {
        val player = NoOpPlayer(color = Color.RED)
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            val occupiedCell = cell.occupy(player)

            Assert.assertTrue(occupiedCell.isOccupiedBy(player))
        }
    }

    @Test
    fun occupy_shouldThrowException_whenCellIsOccupied() {
        val player = NoOpPlayer(color = Color.RED)
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), player),
            MapCell(MapCellType.LAND, Coordinate(1, 2), player),
            MapCell(MapCellType.LAND, Coordinate(3, 2), player),
            MapCell(MapCellType.LAND, Coordinate(1, 4), player)
        )

        cells.forEach { cell ->
            val occupiedCell = kotlin.runCatching { cell.occupy(player) }

            Assert.assertThrows(IllegalStateException::class.java) {
                occupiedCell.getOrThrow()
            }
        }
    }

    @Test
    fun equals_shouldReturnTrue_whenCellHasSameCoordinateAndTypeAndOccupier() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 4)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertEquals(leftCell, rightCell)
        }
    }

    @Test
    fun equals_shouldReturnFalse_whenCellHasDifferentCoordinateOrTypeOrOccupier() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.LAND, Coordinate(0, 1)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.LAND, Coordinate(3, 4)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertNotEquals(leftCell, rightCell)
        }
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenCellHasSameCoordinateAndTypeAndOccupier() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 4)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertEquals(leftCell.hashCode(), rightCell.hashCode())
        }
    }

    @Test
    fun hashCode_shouldReturnDifferentHashCode_whenCellHasDifferentCoordinateOrTypeOrOccupier() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.LAND, Coordinate(0, 1)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2)) to MapCell(MapCellType.LAND, Coordinate(1, 3)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.LAND, Coordinate(3, 4)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4)) to MapCell(MapCellType.LAND, Coordinate(1, 5), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertNotEquals(leftCell.hashCode(), rightCell.hashCode())
        }
    }

    @Test
    fun compareTo_shouldReturnZero_whenCellHasSameCoordinate() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertEquals(0, leftCell.compareTo(rightCell))
        }
    }

    @Test
    fun compareTo_shouldReturnNegative_whenCellHasDifferentCoordinate() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(0, 1)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 3), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 4)),
            MapCell(MapCellType.LAND, Coordinate(1, 4)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 5), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertTrue(leftCell.compareTo(rightCell) < 0)
        }
    }

    @Test
    fun compareTo_shouldReturnPositive_whenCellHasDifferentCoordinate() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 1)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 3)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 4)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 5)) to MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { (leftCell, rightCell) ->
            Assert.assertTrue(leftCell.compareTo(rightCell) > 0)
        }
    }

    @Test
    fun isNonOccupiedAndNonPrizeToken_shouldReturnTrue_whenCellIsNotOccupiedAndNotPrizeToken() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertTrue(cell.isNonOccupiedAndNonPrizeToken())
        }
    }

    @Test
    fun isNonOccupiedAndNonPrizeToken_shouldReturnFalse_whenCellIsOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.PRIZE_TOKEN, Coordinate(1, 4)),
        )

        cells.forEach { cell ->
            Assert.assertFalse(cell.isNonOccupiedAndNonPrizeToken())
        }
    }

    @Test
    fun buildColor_shouldReturnColor_whenCellIsNotOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0)),
            MapCell(MapCellType.LAND, Coordinate(1, 2)),
            MapCell(MapCellType.LAND, Coordinate(3, 2)),
            MapCell(MapCellType.LAND, Coordinate(1, 4))
        )

        cells.forEach { cell ->
            Assert.assertEquals(cell.type.buildColor(null), cell.color)
        }
    }

    @Test
    fun buildColor_shouldReturnColor_whenCellIsOccupied() {
        val cells = listOf(
            MapCell(MapCellType.LAND, Coordinate(0, 0), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(3, 2), NoOpPlayer(color = Color.RED)),
            MapCell(MapCellType.LAND, Coordinate(1, 4), NoOpPlayer(color = Color.RED))
        )

        cells.forEach { cell ->
            Assert.assertEquals(cell.type.buildColor(cell.occupier), cell.color)
        }
    }
}