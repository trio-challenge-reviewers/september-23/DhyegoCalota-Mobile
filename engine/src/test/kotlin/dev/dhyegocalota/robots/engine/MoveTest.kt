package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class MoveTest {

    @Test
    fun origin_shouldReturnOriginCoordinate() {
        val origin = Coordinate(0, 0)
        val move = Move(origin, null)

        Assert.assertEquals(origin, move.origin)
    }

    @Test
    fun direction_shouldReturnDirection_whenDirectionIsNotNull() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)

        Assert.assertEquals(direction, move.direction)
    }

    @Test
    fun direction_shouldReturnNull_whenDirectionIsNull() {
        val origin = Coordinate(0, 0)
        val move = Move(origin, null)

        Assert.assertNull(move.direction)
    }

    @Test
    fun stalemate_shouldReturnTrue_whenDirectionIsNull() {
        val origin = Coordinate(0, 0)
        val move = Move(origin, null)

        Assert.assertTrue(move.stalemate)
    }

    @Test
    fun stalemate_shouldReturnFalse_whenDirectionIsNotNull() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)

        Assert.assertFalse(move.stalemate)
    }

    @Test
    fun destination_shouldReturnOrigin_whenDirectionIsNull() {
        val origin = Coordinate(0, 0)
        val move = Move(origin, null)

        Assert.assertEquals(origin, move.destination)
    }

    @Test
    fun destination_shouldReturnDestination_whenDirectionIsNotNull() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)

        Assert.assertEquals(Coordinate(0, 1), move.destination)
    }

    @Test
    fun equals_shouldReturnTrue_whenObjectsAreTheSame() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)

        Assert.assertTrue(move == move)
    }

    @Test
    fun equals_shouldReturnTrue_whenObjectsAreEqual() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)
        val otherMove = Move(origin, direction)

        Assert.assertTrue(move == otherMove)
    }

    @Test
    fun equals_shouldReturnFalse_whenObjectsAreNotEqual() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)
        val otherMove = Move(origin, MoveDirection.RIGHT)

        Assert.assertFalse(move == otherMove)
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenObjectsAreTheSame() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)

        Assert.assertEquals(move.hashCode(), move.hashCode())
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenObjectsAreEqual() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)
        val otherMove = Move(origin, direction)

        Assert.assertEquals(move.hashCode(), otherMove.hashCode())
    }

    @Test
    fun hashCode_shouldReturnDifferentHashCode_whenObjectsAreNotEqual() {
        val origin = Coordinate(0, 0)
        val direction = MoveDirection.DOWN
        val move = Move(origin, direction)
        val otherMove = Move(origin, MoveDirection.RIGHT)

        Assert.assertNotEquals(move.hashCode(), otherMove.hashCode())
    }
}