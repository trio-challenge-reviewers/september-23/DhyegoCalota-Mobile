package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class AddPlayersToRandomCoordinatesMapTransformerTest {

    @Test
    fun constructor_shouldThrowException_whenNumberOfPlayersIsLessThanOne() {
        val players = setOf<Player>()
        val transformer = kotlin.runCatching { AddPlayersToRandomCoordinatesMapTransformer(players) }

        Assert.assertThrows(IllegalArgumentException::class.java) { transformer.getOrThrow() }
    }

    @Test
    fun transform_shouldThrowException_whenNumberOfPlayersIsGreaterThanNumberOfUnoccupiedCells() {
        val sourceCells = sortedSetOf(
            MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND, occupier = NoOpPlayer(color = Color.BLUE)),
        )

        val players = setOf(
            NoOpPlayer(color = Color.RED),
        )

        val transformer = kotlin.runCatching { AddPlayersToRandomCoordinatesMapTransformer(players).transform(sourceCells) }

        Assert.assertThrows(IllegalArgumentException::class.java) { transformer.getOrThrow() }
    }

    @Test
    fun transform_shouldReturnCellsWithDefinedNumberOfPlayers() {
        val sourceCells = setOf(
            MapCell(
                coordinate = Coordinate(0, 0),
                type = MapCellType.LAND,
                occupier = NoOpPlayer(color = Color.MAGENTA)
            ),
            MapCell(coordinate = Coordinate(0, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 2), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 3), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 4), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 5), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 6), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 7), type = MapCellType.LAND),
            MapCell(
                coordinate = Coordinate(0, 8),
                type = MapCellType.LAND,
                occupier = NoOpPlayer(color = Color.CYAN)
            ),
            MapCell(coordinate = Coordinate(0, 9), type = MapCellType.LAND),
        )

        val setOfTransformedCells = setOf(
            AddPlayersToRandomCoordinatesMapTransformer(setOf(NoOpPlayer(color = Color.RED))) to 1,
            AddPlayersToRandomCoordinatesMapTransformer(
                setOf(
                    NoOpPlayer(color = Color.RED),
                    NoOpPlayer(color = Color.BLUE)
                )
            ) to 2,
            AddPlayersToRandomCoordinatesMapTransformer(
                setOf(
                    NoOpPlayer(color = Color.RED),
                    NoOpPlayer(color = Color.BLUE),
                    NoOpPlayer(color = Color.GREEN)
                )
            ) to 3,
            AddPlayersToRandomCoordinatesMapTransformer(
                setOf(
                    NoOpPlayer(color = Color.RED),
                    NoOpPlayer(color = Color.BLUE),
                    NoOpPlayer(color = Color.GREEN),
                    NoOpPlayer(color = Color.YELLOW)
                )
            ) to 4,
            AddPlayersToRandomCoordinatesMapTransformer(
                setOf(
                    NoOpPlayer(color = Color.RED),
                    NoOpPlayer(color = Color.BLUE),
                    NoOpPlayer(color = Color.GREEN),
                    NoOpPlayer(color = Color.YELLOW),
                    NoOpPlayer(color = Color.PINK)
                )
            ) to 5,
        )

        setOfTransformedCells.forEach { (transformer, numberOfPlayers) ->
            val transformedCells = transformer.transform(sourceCells.toSortedSet())
            val playerCells = transformedCells.filter { transformer.playersToAdd.contains(it.occupier) }

            Assert.assertEquals(numberOfPlayers, playerCells.size)
            Assert.assertTrue(playerCells.all { playerCell -> sourceCells.firstOrNull() { it.coordinate == playerCell.coordinate } != null })
            Assert.assertEquals(transformer.playersToAdd, playerCells.map { it.occupier }.toSet())
        }
    }
}