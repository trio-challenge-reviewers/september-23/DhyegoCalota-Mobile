package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Test

class MoveDirectionTest {

    @Test
    fun values_shouldReturnAllDirections() {
        Assert.assertEquals(4, MoveDirection.values.size)
        Assert.assertTrue(MoveDirection.values.containsAll(setOf(MoveDirection.UP, MoveDirection.RIGHT, MoveDirection.DOWN, MoveDirection.LEFT)))
    }

    @Test
    fun move_whenInvalidDirection_shouldThrowException() {
        val coordinates = listOf(
            Coordinate(0, 0) to MoveDirection.UP,
            Coordinate(0, 0) to MoveDirection.LEFT
        )

        coordinates.forEach { (coordinate, direction) ->
            val modifiedCoordinate = kotlin.runCatching { direction.move(coordinate) }

            Assert.assertThrows(IllegalArgumentException::class.java) {
                modifiedCoordinate.getOrThrow()
            }
        }
    }

    @Test
    fun move_whenValidDirection_shouldReturnNewCoordinate() {
        val coordinates = listOf(
            Coordinate(0, 0) to MoveDirection.RIGHT,
            Coordinate(0, 0) to MoveDirection.DOWN,
            Coordinate(1, 1) to MoveDirection.RIGHT,
            Coordinate(1, 1) to MoveDirection.DOWN
        )

        val expectedCoordinates = listOf(
            Coordinate(1, 0),
            Coordinate(0, 1),
            Coordinate(2, 1),
            Coordinate(1, 2)
        )

        coordinates.forEachIndexed { index, (coordinate, direction) ->
            Assert.assertEquals(expectedCoordinates[index], direction.move(coordinate))
        }
    }

    @Test
    fun equals_shouldReturnTrue_whenComparingSameObject() {
        val directions = listOf(
            MoveDirection.UP to MoveDirection.UP,
            MoveDirection.RIGHT to MoveDirection.RIGHT,
            MoveDirection.DOWN to MoveDirection.DOWN,
            MoveDirection.LEFT to MoveDirection.LEFT
        )

        directions.forEach { (direction, otherDirection) ->
            Assert.assertEquals(direction, otherDirection)
        }
    }

    @Test
    fun equals_shouldReturnFalse_whenComparingDifferentObjects() {
        val directions = listOf(
            MoveDirection.UP to MoveDirection.RIGHT,
            MoveDirection.UP to MoveDirection.DOWN,
            MoveDirection.UP to MoveDirection.LEFT,
            MoveDirection.RIGHT to MoveDirection.UP,
            MoveDirection.RIGHT to MoveDirection.DOWN,
            MoveDirection.RIGHT to MoveDirection.LEFT,
            MoveDirection.DOWN to MoveDirection.UP,
            MoveDirection.DOWN to MoveDirection.RIGHT,
            MoveDirection.DOWN to MoveDirection.LEFT,
            MoveDirection.LEFT to MoveDirection.UP,
            MoveDirection.LEFT to MoveDirection.RIGHT,
            MoveDirection.LEFT to MoveDirection.DOWN
        )

        directions.forEach { (direction, otherDirection) ->
            Assert.assertNotEquals(direction, otherDirection)
        }
    }

    @Test
    fun hashCode_shouldReturnSameValue_whenComparingSameObject() {
        val directions = listOf(
            MoveDirection.UP to MoveDirection.UP,
            MoveDirection.RIGHT to MoveDirection.RIGHT,
            MoveDirection.DOWN to MoveDirection.DOWN,
            MoveDirection.LEFT to MoveDirection.LEFT
        )

        directions.forEach { (direction, otherDirection) ->
            Assert.assertEquals(direction.hashCode(), otherDirection.hashCode())
        }
    }

    @Test
    fun hashCode_shouldReturnDifferentValue_whenComparingDifferentObjects() {
        val directions = listOf(
            MoveDirection.UP to MoveDirection.RIGHT,
            MoveDirection.UP to MoveDirection.DOWN,
            MoveDirection.UP to MoveDirection.LEFT,
            MoveDirection.RIGHT to MoveDirection.UP,
            MoveDirection.RIGHT to MoveDirection.DOWN,
            MoveDirection.RIGHT to MoveDirection.LEFT,
            MoveDirection.DOWN to MoveDirection.UP,
            MoveDirection.DOWN to MoveDirection.RIGHT,
            MoveDirection.DOWN to MoveDirection.LEFT,
            MoveDirection.LEFT to MoveDirection.UP,
            MoveDirection.LEFT to MoveDirection.RIGHT,
            MoveDirection.LEFT to MoveDirection.DOWN
        )

        directions.forEach { (direction, otherDirection) ->
            Assert.assertNotEquals(direction.hashCode(), otherDirection.hashCode())
        }
    }
}