package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class WritablePlayerCursorTest {

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    @Before
    fun setup() {
        firstPlayer = NoOpPlayer(color = Color.RED)
        secondPlayer = NoOpPlayer(color = Color.GREEN)
        thirdPlayer = NoOpPlayer(color = Color.BLUE)
    }

    @Test
    fun constructor_shouldReturnPlayerCursorWithPlayerAndInitialCoordinate() {
        val map = WritableMap(MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer))
        val playerCursor = WritablePlayerCursor(firstPlayer, map)

        val coordinate = playerCursor.currentCoordinate

        Assert.assertEquals(firstPlayer, playerCursor.player)
        Assert.assertEquals(coordinate, playerCursor.initialCoordinate)
    }

    @Test
    fun coordinates_shouldReturnInitialCoordinate_whenPlayerCursorIsCreated() {
        val map = WritableMap(MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer))
        val playerCursor = WritablePlayerCursor(firstPlayer, map)

        val coordinate = playerCursor.currentCoordinate

        Assert.assertEquals(listOf(coordinate), playerCursor.turnHistory.coordinates)
        Assert.assertEquals(coordinate, playerCursor.initialCoordinate)
        Assert.assertEquals(coordinate, playerCursor.currentCoordinate)
    }

    @Test
    fun move_shouldReturnNewCoordinate_whenPlayerCursorMoves() {
        val map = WritableMap(MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer))
        val playerCursor = WritablePlayerCursor(firstPlayer, map)

        val coordinate = playerCursor.currentCoordinate
        val moveCoordinate = playerCursor.move(Turn(1), MoveDirection.RIGHT).destination

        Assert.assertEquals(coordinate, playerCursor.initialCoordinate)
        Assert.assertEquals(Coordinate(1, 0), playerCursor.currentCoordinate)
        Assert.assertEquals(listOf(coordinate, moveCoordinate), playerCursor.turnHistory.coordinates)
    }

    @Test
    fun move_shouldReturnNewCoordinate_whenPlayerCursorMovesTwice() {
        val map = WritableMap(MapCellFactory.twoByTwoCellsForTwoPlayers(firstPlayer, secondPlayer))
        val playerCursor = WritablePlayerCursor(firstPlayer, map)

        val initialCoordinate = playerCursor.currentCoordinate
        val firstMoveCoordinate = playerCursor.move(Turn(1), MoveDirection.DOWN).destination
        val secondMoveCoordinate = playerCursor.move(Turn(2), MoveDirection.DOWN).destination

        Assert.assertEquals(initialCoordinate, playerCursor.initialCoordinate)
        Assert.assertEquals(Coordinate(0, 2), playerCursor.currentCoordinate)
        Assert.assertEquals(listOf(initialCoordinate, firstMoveCoordinate, secondMoveCoordinate), playerCursor.turnHistory.coordinates)
    }

    @Test
    fun move_shouldReturnNewCoordinate_whenPlayerCursorMovesInDifferentDirections() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(secondPlayer, map)

        val initialCoordinate = playerCursor.currentCoordinate
        val firstMoveCoordinate = playerCursor.move(Turn(1), MoveDirection.UP).destination
        val secondMoveCoordinate = playerCursor.move(Turn(2), MoveDirection.RIGHT).destination
        val thirdMoveCoordinate = playerCursor.move(Turn(3), MoveDirection.DOWN).destination
        val fourthMoveCoordinate = playerCursor.move(Turn(4), MoveDirection.LEFT).destination

        Assert.assertEquals(initialCoordinate, playerCursor.initialCoordinate)
        Assert.assertEquals(initialCoordinate, playerCursor.currentCoordinate)
        Assert.assertEquals(listOf(initialCoordinate, firstMoveCoordinate, secondMoveCoordinate, thirdMoveCoordinate, fourthMoveCoordinate), playerCursor.turnHistory.coordinates)
    }

    @Test
    fun move_shouldReturnNewCoordinate_whenPlayerCursorMovesInDifferentDirectionsAndBackToInitial() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(secondPlayer, map)

        val coordinate = playerCursor.currentCoordinate
        val firstMoveCoordinate = playerCursor.move(Turn(1), MoveDirection.UP).destination
        val secondMoveCoordinate = playerCursor.move(Turn(2), MoveDirection.RIGHT).destination
        val thirdMoveCoordinate = playerCursor.move(Turn(3), MoveDirection.DOWN).destination
        val fourthMoveCoordinate = playerCursor.move(Turn(4), MoveDirection.LEFT).destination
        val fifthMoveCoordinate = playerCursor.move(Turn(5), MoveDirection.UP).destination
        val sixthMoveCoordinate = playerCursor.move(Turn(6), MoveDirection.RIGHT).destination
        val seventhMoveCoordinate = playerCursor.move(Turn(7), MoveDirection.DOWN).destination
        val eighthMoveCoordinate = playerCursor.move(Turn(8), MoveDirection.LEFT).destination

        Assert.assertEquals(coordinate, playerCursor.initialCoordinate)
        Assert.assertEquals(coordinate, playerCursor.currentCoordinate)
        Assert.assertEquals(listOf(coordinate, firstMoveCoordinate, secondMoveCoordinate, thirdMoveCoordinate, fourthMoveCoordinate, fifthMoveCoordinate, sixthMoveCoordinate, seventhMoveCoordinate, eighthMoveCoordinate), playerCursor.turnHistory.coordinates)
    }

    @Test
    fun getMovableDirections_shouldReturnAllDirections_whenPlayerCursorIsInCenter() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(secondPlayer, map)

        val movableDirections = playerCursor.getMovableDirections()

        Assert.assertEquals(setOf(MoveDirection.UP, MoveDirection.RIGHT, MoveDirection.DOWN, MoveDirection.LEFT), movableDirections)
    }

    @Test
    fun getMovableDirections_shouldReturnOnlyRightAndDownDirections_whenPlayerCursorIsInTopLeftCorner() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(firstPlayer, map)

        val movableDirections = playerCursor.getMovableDirections()

        Assert.assertEquals(setOf(MoveDirection.RIGHT, MoveDirection.DOWN), movableDirections)
    }

    @Test
    fun getMovableDirections_shouldReturnOnlyLeftAndDownDirections_whenPlayerCursorIsInBottomRightCorner() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(thirdPlayer, map)

        val movableDirections = playerCursor.getMovableDirections()

        Assert.assertEquals(setOf(MoveDirection.LEFT, MoveDirection.UP), movableDirections)
    }

    @Test
    fun getMovableDirections_shouldReturnEmpty_whenAllOtherCellsAreOccupied() {
        val map = WritableMap(MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer))
        val playerCursor = WritablePlayerCursor(thirdPlayer, map)

        val cellsNotOccupiedByBot = map.cells.filter { it.occupier != thirdPlayer }
        cellsNotOccupiedByBot.forEach { cell ->
            map.changeCell(cell.copy(occupier = thirdPlayer))
        }

        val movableDirections = playerCursor.getMovableDirections()

        Assert.assertEquals(emptySet<MoveDirection>(), movableDirections)
    }
}