package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import dev.dhyegocalota.robots.engine.helper.ChannelReader
import dev.dhyegocalota.robots.engine.helper.CoroutineScopeBuilder
import dev.dhyegocalota.robots.engine.helper.TimeoutReader
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GameEngineTest {

    private lateinit var logger: Logger

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    private lateinit var players: Set<Player>

    private var didStart: Boolean = false
    private var didStop: Boolean = false
    private var winner: Player? = null
    private var turnsCounted = mutableListOf<Turn>()

    private lateinit var onStart: GameObserver
    private lateinit var onStop: GameObserver
    private lateinit var onWinner: GameWinnerObserver
    private lateinit var onTurn: TurnClockObserver

    private lateinit var boundaries: Boundaries
    private lateinit var mapBuilder: MapBuilder
    private lateinit var gameEngine: GameEngine

    @Before
    fun setup() {
        logger = PrintLnLogger()

        firstPlayer = NoOpPlayer(Color.RED)
        secondPlayer = NoOpPlayer(Color.GREEN)
        thirdPlayer = NoOpPlayer(Color.BLUE)

        players = setOf(firstPlayer, secondPlayer, thirdPlayer)

        didStart = false
        didStop = false
        turnsCounted = mutableListOf()

        onStart = { didStart = true }
        onStop = { didStop = true }
        onWinner = { winner = it }
        onTurn = { turnsCounted.add(it) }

        boundaries = Boundaries(10, 10)
        mapBuilder = MapBuilder()
            .withBoundaries(boundaries)

        gameEngine = GameEngine(
            logger = logger,
            players = players,
            mapBuilder = mapBuilder,
            onStart = onStart,
            onStop = onStop,
            onWinner = onWinner,
            onTurn = onTurn,
        )
    }

    @Test
    fun constructor_shouldReturnEmptyGames() {
        Assert.assertTrue(gameEngine.games.isEmpty())
    }

    @Test
    fun games_shouldReturnGames_afterStart() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        gameEngine.stop()

        Assert.assertTrue(gameEngine.games.isNotEmpty())
    }

    @Test
    fun currentGame_shouldReturnNull_whenNoGameIsRunning() {
        Assert.assertNull(gameEngine.currentGame)
    }

    @Test
    fun currentGame_shouldReturnCurrentGame_whenGameIsRunning() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        val currentGame = gameEngine.currentGame
        gameEngine.stop()

        Assert.assertNotNull(currentGame)
    }

    @Test
    fun isRunning_shouldReturnFalse_whenNoGameIsRunning() {
        Assert.assertFalse(gameEngine.isRunning)
    }

    @Test
    fun isRunning_shouldReturnTrue_whenGameIsRunning() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        val wasRunning = gameEngine.isRunning
        gameEngine.stop()

        Assert.assertTrue(wasRunning)
    }

    @Test
    fun rank_shouldReturnEmptyRank_whenNoGameIsRunning() {
        Assert.assertTrue(gameEngine.rank.playersRank.isEmpty())
    }

    @Test
    fun rank_shouldReturnRank_whenAtLeastOneGameIsWon() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, 2)

        val firstGame = gameEngine.currentGame
        val map = firstGame!!.map
        val arbitraryCell = map.cells.first { it.isPrizeToken() }
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = secondPlayer)
        map.changeCell(arbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, 1)
        gameEngine.stop()

        Assert.assertEquals(listOf(secondPlayer to 1, firstPlayer to 0, thirdPlayer to 0), gameEngine.rank.playersRankSorted)
    }

    @Test
    fun elapsedDuration_shouldReturnZero_whenNoGameIsRunning() {
        Assert.assertEquals(0, gameEngine.elapsedDuration.inWholeMilliseconds)
    }

    @Test
    fun elapsedDuration_shouldReturnElapsedTime_whenGameIsRunning() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        Assert.assertTrue(gameEngine.elapsedDuration.inWholeMilliseconds > 0)
    }

    @Test
    fun elapsedDuration_shouldKeepAddingElapsedTime_whenGameStartedAgain() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        var turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        val firstElapsedDuration = gameEngine.elapsedDuration.inWholeMilliseconds

        turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        gameEngine.stop()

        val secondElapsedDuration = gameEngine.elapsedDuration.inWholeMilliseconds

        turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        gameEngine.stop()

        val thirdElapsedDuration = gameEngine.elapsedDuration.inWholeMilliseconds

        Assert.assertTrue(firstElapsedDuration < secondElapsedDuration)
        Assert.assertTrue(secondElapsedDuration < thirdElapsedDuration)
    }

    @Test
    fun onStart_shouldBeCalled_whenGameStarts() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        Assert.assertTrue(didStart)
    }

    @Test
    fun onStop_shouldBeCalled_whenGameStops() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        Assert.assertTrue(didStop)
    }

    @Test
    fun onTurn_shouldBeCalled_whenGameTurns() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        Assert.assertEquals(3, turnsCounted.size)
        Assert.assertEquals(listOf(Turn(1), Turn(2), Turn(3)), turnsCounted)
    }

    @Test
    fun onWinner_shouldStartNewGames_whenCurrentGameEnds() = runTest {
        val turnCountToWait = 3

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = gameEngine.start(gameScope)

        ChannelReader.read(turnsChannel, turnCountToWait)

        val firstGame = gameEngine.currentGame!!
        val firstGameMap = firstGame.map
        val firstGameArbitraryCell = firstGameMap.cells.first { it.isPrizeToken() }
        val firstGameArbitraryPrizeTokenCell = firstGameArbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = secondPlayer)
        firstGameMap.changeCell(firstGameArbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, turnCountToWait)

        TimeoutReader.blockUntilCondition(timeout = gameEngine.config.intervalSpeed) { gameEngine.games.size == 2 }
        val secondGame = gameEngine.currentGame!!

        ChannelReader.read(turnsChannel, turnCountToWait)

        val secondGameMap = secondGame.map
        val secondGameArbitraryCell = secondGameMap.cells.first { it.isPrizeToken() }
        val secondGameArbitraryPrizeTokenCell = secondGameArbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = secondPlayer)
        secondGameMap.changeCell(secondGameArbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, turnCountToWait)

        TimeoutReader.blockUntilCondition(timeout = gameEngine.config.intervalSpeed) { gameEngine.games.size == 3 }
        val thirdGame = gameEngine.currentGame!!

        ChannelReader.read(turnsChannel, turnCountToWait)

        gameEngine.stop()

        Assert.assertNotNull(winner)
        Assert.assertEquals(3, gameEngine.games.size)
        Assert.assertNotEquals(firstGame, secondGame)
        Assert.assertNotEquals(secondGame, thirdGame)
        Assert.assertNotEquals(firstGame, thirdGame)
        Assert.assertEquals(15, turnsCounted.size)
        Assert.assertEquals(listOf(1..4, 1..6, 1..5).map { it.map { Turn(it) } }.flatten(), turnsCounted)
    }

    @Test
    fun start_shouldContinuePreviousGame_whenGameStopped() = runTest {
        val turnCountToWait = 3
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        var turnsChannel = gameEngine.start(gameScope)
        val initialGame = gameEngine.currentGame!!

        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        turnsChannel = gameEngine.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)
        gameEngine.stop()

        Assert.assertEquals(initialGame, gameEngine.currentGame)
    }
}