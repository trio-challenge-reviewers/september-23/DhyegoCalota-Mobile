package dev.dhyegocalota.robots.engine.config

import dev.dhyegocalota.robots.engine.Logger

class PrintLnLogger : Logger {
    override fun verbose(tag: String, message: String) = log(tag, message)

    override fun debug(tag: String, message: String) = log(tag, message)

    override fun info(tag: String, message: String) = log(tag, message)

    override fun warning(tag: String, message: String) = log(tag, message)

    override fun error(tag: String, message: String) = log(tag, message)

    private fun log(tag: String, message: String) = println("$tag: $message")
}
