package dev.dhyegocalota.robots.engine

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.SortedSet

class MapTransformPipelineTest {
    private lateinit var removeLastCell: MapTransformer
    private lateinit var removeFirstCell: MapTransformer
    private lateinit var placePrizeTokenOnSecondCell: MapTransformer

    @Before
    fun setup() {
        removeLastCell = object : MapTransformer {
            override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
                cells.remove(cells.last())
                return cells
            }
        }

        removeFirstCell = object : MapTransformer {
            override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
                cells.remove(cells.first())
                return cells
            }
        }

        placePrizeTokenOnSecondCell = object : MapTransformer {
            override fun transform(cells: SortedSet<MapCell>): SortedSet<MapCell> {
                val sourceCell = cells.elementAt(1)
                val prizeToken = sourceCell.copy(type = MapCellType.PRIZE_TOKEN)
                cells.remove(sourceCell)
                cells.add(prizeToken)
                return cells
            }
        }
    }

    @Test
    fun transform_shouldCallAllTransformers() {
        val sourceCells = sortedSetOf(
            MapCell(coordinate = Coordinate(0, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 2), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 3), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(0, 4), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 2), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 3), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(1, 4), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(2, 0), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(2, 1), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(2, 2), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(2, 3), type = MapCellType.LAND),
            MapCell(coordinate = Coordinate(2, 4), type = MapCellType.LAND),
        )

        val pipeline = MapTransformPipeline(
            setOf(
                removeLastCell,
                placePrizeTokenOnSecondCell,
                removeFirstCell
            )
        )

        val transformedCells = pipeline.transform(sourceCells)

        Assert.assertEquals(13, transformedCells.size)
        Assert.assertEquals(MapCellType.PRIZE_TOKEN, transformedCells.elementAt(0).type)
    }
}