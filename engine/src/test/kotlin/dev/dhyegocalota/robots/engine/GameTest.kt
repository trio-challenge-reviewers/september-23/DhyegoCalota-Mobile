package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import dev.dhyegocalota.robots.engine.helper.ChannelReader
import dev.dhyegocalota.robots.engine.helper.CoroutineScopeBuilder
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.time.Duration.Companion.milliseconds

class GameTest {

    private lateinit var logger: Logger

    private lateinit var firstPlayer: NoOpPlayer
    private lateinit var secondPlayer: NoOpPlayer
    private lateinit var thirdPlayer: NoOpPlayer

    private lateinit var onePlayers: Set<Player>
    private lateinit var twoPlayers: Set<Player>
    private lateinit var threePlayers: Set<Player>

    private lateinit var oneByTwoCellsForOnePlayers: Set<MapCell>
    private lateinit var twoByTwoCellsForTwoPlayers: Set<MapCell>
    private lateinit var threeByFourCellsForThreePlayers: Set<MapCell>

    private var winner: Player? = null
    private var turnsCounted = mutableSetOf<Turn>()

    private lateinit var gameWinnerObserver: GameWinnerObserver
    private lateinit var turnObserver: TurnClockObserver

    @Before
    fun setUp() {
        logger = PrintLnLogger()

        firstPlayer = NoOpPlayer(Color.RED)
        secondPlayer = NoOpPlayer(Color.GREEN)
        thirdPlayer = NoOpPlayer(Color.BLUE)

        onePlayers = setOf(firstPlayer)
        twoPlayers = setOf(firstPlayer, secondPlayer)
        threePlayers = setOf(firstPlayer, secondPlayer, thirdPlayer)

        oneByTwoCellsForOnePlayers = MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer)
        twoByTwoCellsForTwoPlayers = MapCellFactory.twoByTwoCellsForTwoPlayers(firstPlayer, secondPlayer)
        threeByFourCellsForThreePlayers = MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer)

        gameWinnerObserver = { winner ->
            this.winner = winner
        }

        turnObserver = { turn ->
            turnsCounted.add(turn)
        }
    }

    @After
    fun tearDown() {
        winner = null
        turnsCounted = mutableSetOf()
    }

    @Test
    fun constructor_shouldReturnGameWithPlayers() {
        val map = WritableMap(
            initialCells = twoByTwoCellsForTwoPlayers,
        )

        val game = Game(
            logger = logger,
            map = map,
            players = onePlayers
        )

        Assert.assertEquals(onePlayers, game.players)
    }

    @Test
    fun constructor_shouldReturnPlayerCursors() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers
        )

        val expectedPlayerCursors = listOf(
            WritablePlayerCursor(firstPlayer, game.map),
            WritablePlayerCursor(secondPlayer, game.map),
            WritablePlayerCursor(thirdPlayer, game.map),
        )

        expectedPlayerCursors.forEach { expectedPlayerCursor ->
            val existingCursor = game.playerCursors.first { it.player == expectedPlayerCursor.player }
            Assert.assertEquals(existingCursor.turnHistory.coordinates, expectedPlayerCursor.turnHistory.coordinates)
        }
    }

    @Test
    fun constructor_shouldReturnGameWithMap() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers
        )

        Assert.assertEquals(threeByFourCellsForThreePlayers, game.map.cells)
        Assert.assertEquals("3x4", game.map.boundaries.toString())
    }

    @Test
    fun constructor_shouldReturnGameWithDefaultConfig_whenConfigIsNotProvided() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver
        )

        Assert.assertEquals(GameConfig.DEFAULT, game.config)
    }

    @Test
    fun constructor_shouldReturnGameWithProvidedConfig_whenConfigIsProvided() {
        val config = GameConfig(intervalSpeed = 1000.milliseconds)
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
            config = config,
        )

        Assert.assertEquals(config, game.config)
    }

    @Test
    fun constructor_shouldReturnGameWithNoWinner_whenGameIsCreated() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        Assert.assertNull(game.winner)
        Assert.assertFalse(game.isGameOver)
    }

    @Test
    fun constructor_shouldReturnGameWithIsRunningFalse_whenGameIsCreated() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        Assert.assertFalse(game.isRunning)
    }

    @Test
    fun constructor_shouldThrowException_whenPlayersHaveReservedColors() {
        val players = setOf(
            NoOpPlayer(Color.YELLOW),
            NoOpPlayer(Color.RED),
            NoOpPlayer(Color.GREEN)
        )

        val initialCells = MapCellFactory.threeByFourCellsForThreePlayers(
            firstPlayer = players.first(),
            secondPlayer = players.elementAt(1),
            thirdPlayer = players.elementAt(2)
        )

        val map = WritableMap(
            initialCells = initialCells,
        )

        val exception = kotlin.runCatching {
            Game(
                logger,
                map = map,
                players = players,
                onWinner = gameWinnerObserver,
                onTurn = turnObserver,
            )
        }

        Assert.assertThrows(IllegalArgumentException::class.java) { exception.getOrThrow() }
    }

    @Test
    fun constructor_shouldThrowException_whenPlayersIsEmpty() {
        val map = WritableMap(
            initialCells = twoByTwoCellsForTwoPlayers,
        )

        val game = kotlin.runCatching {
            Game(
                logger = logger,
                map = map,
                players = emptySet(),
            )
        }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            game.getOrThrow()
        }
    }

    @Test
    fun constructor_shouldThrowException_whenPrizeTokensIsEmpty() {
        val game = kotlin.runCatching {
            val mapWithoutPrizeToken =
                threeByFourCellsForThreePlayers.map {
                    val type = if (it.isPrizeToken()) MapCellType.LAND else it.type
                    it.copy(type = type)
                }

            val map = WritableMap(
                initialCells = mapWithoutPrizeToken.toSet(),
            )

            Game(
                logger = logger,
                map = map,
                players = onePlayers,
            )
        }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            game.getOrThrow()
        }
    }

    @Test
    fun constructor_shouldThrowException_whenPlayersDoesNotHaveOneOccupiedCell() {
        val game = kotlin.runCatching {
            val mapWithoutOccupiedCell =
                threeByFourCellsForThreePlayers.map {
                    it.copy(occupier = null)
                }

            val map = WritableMap(
                initialCells = mapWithoutOccupiedCell.toSet(),
            )

            Game(
                logger = logger,
                map = map,
                players = onePlayers
            )
        }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            game.getOrThrow()
        }
    }

    @Test
    fun start_shouldStartGame() = runTest {
        val turnCountToWait = 2
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        var wasRunning = game.isRunning
        game.stop()

        Assert.assertTrue(wasRunning)
        Assert.assertEquals(turnCountToWait, turnsCounted.size)
        Assert.assertEquals(setOf(Turn(1), Turn(2)), turnsCounted)
    }

    @Test
    fun start_shouldThrowException_whenGameIsOver() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)
        ChannelReader.read(turnsChannel, 3)

        val arbitraryCell = game.map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = game.players.elementAt(1))
        game.map.changeCell(arbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, 1)

        val start = kotlin.runCatching {
            val gameScope = CoroutineScopeBuilder.buildGameScope()
            game.start(gameScope)
        }

        Assert.assertTrue(game.isGameOver)
        Assert.assertThrows(IllegalStateException::class.java) { start.getOrThrow() }
    }

    @Test
    fun stop_shouldStopGame() = runTest {
        val turnCountToWait = 2
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)
        ChannelReader.read(turnsChannel, turnCountToWait)

        game.stop()

        Assert.assertFalse(game.isRunning)
        Assert.assertEquals(turnCountToWait, turnsCounted.size)
        Assert.assertEquals(setOf(Turn(1), Turn(2)), turnsCounted)
    }

    @Test
    fun stop_shouldNotResetGame() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 2)

        game.stop()

        Assert.assertEquals(Turn(2), game.clock.currentTurn)
        Assert.assertEquals(setOf(Turn(1), Turn(2)), turnsCounted)
    }

    @Test
    fun onEnd_shouldSetWinner_whenGameEnds() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 2)

        val arbitraryCell = game.map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = game.players.elementAt(1))
        game.map.changeCell(arbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, 1)

        Assert.assertEquals(game.players.elementAt(1), game.winner)
        Assert.assertEquals(3, turnsCounted.size)
    }

    @Test
    fun onEnd_shouldNotSetWinner_whenGameEndsAndThereIsNoWinner() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 3)

        val arbitraryCell = game.map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN)
        game.map.changeCell(arbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, 2)

        game.stop()

        Assert.assertNull(game.winner)
        Assert.assertEquals(5, turnsCounted.size)
    }

    @Test
    fun onEnd_shouldStopGame_whenGameEnds() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 2)

        val arbitraryCell = game.map.getCell(Coordinate(2, 1))
        val arbitraryPrizeTokenCell = arbitraryCell.copy(type = MapCellType.PRIZE_TOKEN, occupier = game.players.elementAt(1))
        game.map.changeCell(arbitraryPrizeTokenCell)

        ChannelReader.read(turnsChannel, 1)

        Assert.assertFalse(game.isRunning)
        Assert.assertEquals(3, turnsCounted.size)
    }

    @Test
    fun onTurn_shouldIncrementTurn_whenGameIsRunning() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 2)

        game.stop()

        Assert.assertEquals(Turn(2), game.clock.currentTurn)
        Assert.assertEquals(2, turnsCounted.size)
        Assert.assertEquals(setOf(Turn(1), Turn(2)), turnsCounted)
    }

    @Test
    fun onTurn_shouldMakePlayerMove_whenGameIsRunning() = runTest {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger,
            map = map,
            players = threePlayers,
            onWinner = gameWinnerObserver,
            onTurn = turnObserver,
        )

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val turnsChannel = game.start(gameScope)

        ChannelReader.read(turnsChannel, 4)

        game.stop()

        Assert.assertEquals(2, firstPlayer.numberOfMoves)
        Assert.assertEquals(1, secondPlayer.numberOfMoves)
        Assert.assertEquals(1, thirdPlayer.numberOfMoves)
    }

    @Test
    fun movePlayer_shouldMovePlayer() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers,
        )

        val game = Game(
            logger = logger,
            map = map,
            players = onePlayers
        )

        val playerCursor = game.playerCursors.first { it.player == firstPlayer }
        val initialCoordinate = playerCursor.initialCoordinate
        val expectedCoordinate = Coordinate(initialCoordinate.x + 1, initialCoordinate.y)

        game.movePlayer(Turn(1), firstPlayer, MoveDirection.RIGHT)

        val actualCoordinate = playerCursor.currentCoordinate

        Assert.assertEquals(expectedCoordinate, actualCoordinate)
        Assert.assertEquals(listOf(initialCoordinate, expectedCoordinate), playerCursor.turnHistory.coordinates)
    }

    @Test
    fun movePlayer_shouldThrowException_whenPlayerIsNotPartOfMap() {
        val map = WritableMap(
            initialCells = oneByTwoCellsForOnePlayers,
        )

        val game = Game(
            logger = logger,
            map = map,
            players = onePlayers
        )

        val exception = kotlin.runCatching { game.movePlayer(Turn(1), secondPlayer, MoveDirection.RIGHT) }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            exception.getOrThrow()
        }
    }

    @Test
    fun movePlayer_shouldThrowException_whenMoveIsNotAllowed() {
        val map = WritableMap(
            initialCells = threeByFourCellsForThreePlayers
        )

        val game = Game(
            logger = logger,
            map = map,
            players = threePlayers
        )

        val exception = kotlin.runCatching { game.movePlayer(Turn(1), firstPlayer, MoveDirection.LEFT) }

        Assert.assertThrows(IllegalArgumentException::class.java) {
            exception.getOrThrow()
        }
    }
}