package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class PlayerTest {

    @Test
    fun construct_shouldReturnPlayerWithColor() {
        val player = NoOpPlayer(color = Color.RED)

        Assert.assertEquals(Color.RED, player.color)
    }

    @Test
    fun equals_shouldReturnTrue_whenPlayersHaveSameColor() {
        val firstPlayer = NoOpPlayer(color = Color.RED)
        val secondPlayer = NoOpPlayer(color = Color.RED)

        Assert.assertEquals(firstPlayer, secondPlayer)
    }

    @Test
    fun equals_shouldReturnFalse_whenPlayersHaveDifferentColor() {
        val firstPlayer = NoOpPlayer(color = Color.RED)
        val secondPlayer = NoOpPlayer(color = Color.BLUE)

        Assert.assertNotEquals(firstPlayer, secondPlayer)
    }

    @Test
    fun hashCode_shouldReturnSameHashCode_whenPlayersHaveSameColor() {
        val firstPlayer = NoOpPlayer(color = Color.RED)
        val secondPlayer = NoOpPlayer(color = Color.RED)

        Assert.assertEquals(firstPlayer.hashCode(), secondPlayer.hashCode())
    }

    @Test
    fun hashCode_shouldReturnDifferentHashCode_whenPlayersHaveDifferentColor() {
        val firstPlayer = NoOpPlayer(color = Color.RED)
        val secondPlayer = NoOpPlayer(color = Color.BLUE)

        Assert.assertNotEquals(firstPlayer.hashCode(), secondPlayer.hashCode())
    }
}