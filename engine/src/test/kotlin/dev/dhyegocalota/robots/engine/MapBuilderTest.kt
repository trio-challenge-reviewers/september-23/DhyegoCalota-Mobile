package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class MapBuilderTest {

    @Test
    fun construct_shouldThrowException_whenPlayersIsEmpty() {
        val map = kotlin.runCatching { MapBuilder().withBoundaries(Boundaries(1, 1)).build() }

        Assert.assertThrows(IllegalArgumentException::class.java) { map.getOrThrow() }
    }

    @Test
    fun construct_shouldThrowException_whenBoundariesIsNotDefined() {
        val map = kotlin.runCatching { MapBuilder().build() }

        Assert.assertThrows(IllegalArgumentException::class.java) { map.getOrThrow() }
    }

    @Test
    fun construct_shouldThrowException_whenNumberOfPrizeTokensIsLessThanOne() {
        val map = kotlin.runCatching { MapBuilder().withNumberOfPrizeTokens(0).build() }

        Assert.assertThrows(IllegalArgumentException::class.java) { map.getOrThrow() }
    }

    @Test
    fun build_shouldReturnMapWithDefinedBoundaries() {
        val boundaries = Boundaries(2, 2)
        val map = MapBuilder()
            .withPlayer(NoOpPlayer(color = Color.RED))
            .withBoundaries(boundaries)
            .build()

        Assert.assertEquals(boundaries, map.boundaries)
    }

    @Test
    fun build_shouldReturnMapWithDefinedNumberOfPrizeTokens() {
        val numberOfPrizeTokens = 1
        val map = MapBuilder()
            .withPlayer(NoOpPlayer(color = Color.RED))
            .withBoundaries(Boundaries(2, 1))
            .withNumberOfPrizeTokens(numberOfPrizeTokens)
            .build()

        Assert.assertEquals(numberOfPrizeTokens, map.cells.filter { it.isPrizeToken() }.size)
    }

    @Test
    fun build_shouldThrowException_whenNumberOfPrizeTokensIsGreaterThanNumberOfUnoccupiedCells() {
        val map = kotlin.runCatching {
            MapBuilder()
                .withPlayer(NoOpPlayer(color = Color.RED))
                .withBoundaries(Boundaries(1, 1))
                .withNumberOfPrizeTokens(2)
                .build()
        }

        Assert.assertThrows(IllegalArgumentException::class.java) { map.getOrThrow() }
    }

    @Test
    fun build_shouldThrowException_whenNumberOfPlayersIsGreaterThanNumberOfUnoccupiedCells() {
        val map = kotlin.runCatching {
            MapBuilder()
                .withPlayer(NoOpPlayer(color = Color.RED))
                .withPlayer(NoOpPlayer(color = Color.BLUE))
                .withBoundaries(Boundaries(1, 1))
                .build()
        }

        Assert.assertThrows(IllegalArgumentException::class.java) { map.getOrThrow() }
    }

    @Test
    fun build_shouldReturnDifferentMaps_whenCalledMultipleTimes() {
        val player = NoOpPlayer(color = Color.RED)
        val boundaries = Boundaries(30, 30)

        val firstMap = MapBuilder()
            .withPlayer(player)
            .withBoundaries(boundaries)
            .build()

        val secondMap = MapBuilder()
            .withPlayer(player)
            .withBoundaries(boundaries)
            .build()

        Assert.assertNotEquals(firstMap, secondMap)
    }
}