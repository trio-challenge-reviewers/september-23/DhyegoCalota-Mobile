package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import dev.dhyegocalota.robots.engine.helper.CoroutineScopeBuilder
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GamesRankTest {

    private lateinit var logger: Logger

    private lateinit var firstPlayer: Player
    private lateinit var secondPlayer: Player
    private lateinit var thirdPlayer: Player

    private lateinit var threePlayers: Set<Player>

    private lateinit var gameWithOnePlayer: Game
    private lateinit var gameWithTwoPlayers: Game
    private lateinit var gameWithThreePlayers: Game

    @Before
    fun setup() {
        logger = PrintLnLogger()

        firstPlayer = NoOpPlayer(Color.RED)
        secondPlayer = NoOpPlayer(Color.GREEN)
        thirdPlayer = NoOpPlayer(Color.BLUE)

        val onePlayer = setOf(firstPlayer)
        val initialCellsWithOnePlayer = MapCellFactory.oneByTwoCellsForOnePlayers(firstPlayer)

        val twoPlayers = setOf(firstPlayer, secondPlayer)
        val initialCellsWithTwoPlayers = MapCellFactory.twoByTwoCellsForTwoPlayers(
            firstPlayer = firstPlayer,
            secondPlayer = secondPlayer
        )

        threePlayers = setOf(firstPlayer, secondPlayer, thirdPlayer)
        val initialCellsWithThreePlayers = MapCellFactory.threeByFourCellsForThreePlayers(
            firstPlayer = firstPlayer,
            secondPlayer = secondPlayer,
            thirdPlayer = thirdPlayer
        )

        val mapWithOnePlayer = WritableMap(initialCells = initialCellsWithOnePlayer)
        val mapWithTwoPlayers = WritableMap(initialCells = initialCellsWithTwoPlayers)
        val mapWithThreePlayers = WritableMap(initialCells = initialCellsWithThreePlayers)

        gameWithOnePlayer = Game(
            logger = logger,
            players = onePlayer,
            map = mapWithOnePlayer,
            onWinner = {},
            onTurn = {},
        )

        gameWithTwoPlayers = Game(
            logger = logger,
            players = twoPlayers,
            map = mapWithTwoPlayers,
            onWinner = {},
            onTurn = {},
        )

        gameWithThreePlayers = Game(
            logger = logger,
            players = threePlayers,
            map = mapWithThreePlayers,
            onWinner = {},
            onTurn = {},
        )
    }

    @Test
    fun construct_shouldReturnGames() {
        val games = setOf<Game>()
        val gamesRank = GamesRank(games)

        Assert.assertEquals(games, gamesRank.games)
    }

    @Test
    fun players_shouldReturnEmptySet_whenGamesIsEmpty() {
        val games = setOf<Game>()
        val gamesRank = GamesRank(games)

        Assert.assertEquals(emptySet<Player>(), gamesRank.players)
    }

    @Test
    fun players_shouldReturnPlayersFromGames() {
        val games = setOf(
            gameWithOnePlayer,
            gameWithTwoPlayers,
            gameWithThreePlayers
        )

        val gamesRank = GamesRank(games)

        Assert.assertEquals(threePlayers, gamesRank.players)
    }

    @Test
    fun playersRank_shouldReturnEmptyMap_whenGamesIsEmpty() {
        val games = setOf<Game>()
        val gamesRank = GamesRank(games)

        Assert.assertEquals(emptyMap<Player, Int>(), gamesRank.playersRank)
    }

    @Test
    fun playersRank_shouldReturnPlayersWithWinsCount() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val games = setOf(
            gameWithOnePlayer,
            gameWithTwoPlayers,
            gameWithThreePlayers
        )

        gameWithOnePlayer.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates
        gameWithOnePlayer.clock.nextTurn(gameScope) // Second Player Turn to Detect Initial Coordinates
        gameWithOnePlayer.map.changeCell(
            gameWithOnePlayer.map.cells.first { it.isPrizeToken() }.copy(
                occupier = firstPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithOnePlayer.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates

        gameWithTwoPlayers.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates
        gameWithTwoPlayers.map.changeCell(
            gameWithTwoPlayers.map.cells.first { it.isPrizeToken() }.copy(
                occupier = firstPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithTwoPlayers.clock.nextTurn(gameScope) // Second Player Turn to Detect Initial Coordinates

        gameWithThreePlayers.clock.nextTurn(gameScope)  // First Player Turn to Detect Initial Coordinates
        gameWithThreePlayers.map.changeCell(
            gameWithThreePlayers.map.cells.first { it.isPrizeToken() }.copy(
                occupier = secondPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithThreePlayers.clock.nextTurn(gameScope)  // Second Player Turn to Detect Initial Coordinates

        val gamesRank = GamesRank(games)

        Assert.assertEquals(
            mapOf(
                firstPlayer to 2,
                secondPlayer to 1,
                thirdPlayer to 0
            ),
            gamesRank.playersRank
        )
    }

    @Test
    fun playersRankSorted_shouldReturnEmptyList_whenGamesIsEmpty() {
        val games = setOf<Game>()
        val gamesRank = GamesRank(games)

        Assert.assertEquals(emptyList<Pair<Player, Int>>(), gamesRank.playersRankSorted)
    }

    @Test
    fun playersRankSorted_shouldReturnPlayersWithWinsCountSorted() = runTest {
        val gameScope = CoroutineScopeBuilder.buildGameScope()
        val games = setOf(
            gameWithOnePlayer,
            gameWithTwoPlayers,
            gameWithThreePlayers
        )

        gameWithOnePlayer.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates
        gameWithOnePlayer.clock.nextTurn(gameScope) // Second Player Turn to Detect Initial Coordinates
        gameWithOnePlayer.map.changeCell(
            gameWithOnePlayer.map.cells.first { it.isPrizeToken() }.copy(
                occupier = firstPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithOnePlayer.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates

        gameWithTwoPlayers.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates
        gameWithTwoPlayers.map.changeCell(
            gameWithTwoPlayers.map.cells.first { it.isPrizeToken() }.copy(
                occupier = secondPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithTwoPlayers.clock.nextTurn(gameScope) // Second Player Turn to Detect Initial Coordinates

        gameWithThreePlayers.clock.nextTurn(gameScope) // First Player Turn to Detect Initial Coordinates
        gameWithThreePlayers.map.changeCell(
            gameWithThreePlayers.map.cells.first { it.isPrizeToken() }.copy(
                occupier = secondPlayer,
                type = MapCellType.PRIZE_TOKEN
            )
        )
        gameWithThreePlayers.clock.nextTurn(gameScope) // Second Player Turn to Detect Initial Coordinates

        val gamesRank = GamesRank(games)

        Assert.assertEquals(
            listOf(
                secondPlayer to 2,
                firstPlayer to 1,
                thirdPlayer to 0
            ),
            gamesRank.playersRankSorted
        )
    }
}