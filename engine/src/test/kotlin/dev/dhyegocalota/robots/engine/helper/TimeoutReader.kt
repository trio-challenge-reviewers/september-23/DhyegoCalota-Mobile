package dev.dhyegocalota.robots.engine.helper

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

object TimeoutReader {
    suspend fun blockUntilCondition(timeout: Duration, checkInterval: Duration = 100.milliseconds, condition: suspend () -> Boolean) = runBlocking {
        val latch = CountDownLatch(1)

        GlobalScope.launch {
            while (!condition()) {
                delay(checkInterval)
            }
            latch.countDown()
        }

        latch.await(timeout.inWholeMilliseconds, TimeUnit.MILLISECONDS)
    }
}