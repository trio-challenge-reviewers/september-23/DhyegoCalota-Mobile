package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import org.junit.Assert
import org.junit.Test

class MapCellTypeTest {

    @Test
    fun isPrizeToken_shouldReturnTrue_whenCellTypeIsPrizeToken() {
        Assert.assertTrue(MapCellType.PRIZE_TOKEN.isPrizeToken)
    }

    @Test
    fun isPrizeToken_shouldReturnFalse_whenCellTypeIsNotPrizeToken() {
        Assert.assertFalse(MapCellType.LAND.isPrizeToken)
    }

    @Test
    fun isLand_shouldReturnTrue_whenCellTypeIsLand() {
        Assert.assertTrue(MapCellType.LAND.isLand)
    }

    @Test
    fun isLand_shouldReturnFalse_whenCellTypeIsNotLand() {
        Assert.assertFalse(MapCellType.PRIZE_TOKEN.isLand)
    }

    @Test
    fun buildColor_shouldReturnYellow_whenCellTypeIsPrizeToken() {
        val occupier = NoOpPlayer(Color.RED)
        Assert.assertEquals(Color.YELLOW, MapCellType.PRIZE_TOKEN.buildColor(null))
        Assert.assertEquals(Color.YELLOW, MapCellType.PRIZE_TOKEN.buildColor(occupier))
    }

    @Test
    fun buildColor_shouldReturnGray_whenCellTypeIsLandAndOccupierIsNull() {
        Assert.assertEquals(Color.GRAY, MapCellType.LAND.buildColor(null))
    }

    @Test
    fun buildColor_shouldReturnOccupierColor_whenCellTypeIsLandAndOccupierIsNotNull() {
        val occupier = NoOpPlayer(Color.RED)
        Assert.assertEquals(Color.RED, MapCellType.LAND.buildColor(occupier))
    }
}