package dev.dhyegocalota.robots.engine

import dev.dhyegocalota.robots.engine.config.NoOpPlayer
import dev.dhyegocalota.robots.engine.config.NoOpTurnControl
import dev.dhyegocalota.robots.engine.config.PrintLnLogger
import dev.dhyegocalota.robots.engine.factory.MapCellFactory
import dev.dhyegocalota.robots.engine.helper.CoroutineScopeBuilder
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.time.Duration

class TurnControlTest {

    private lateinit var clock: TurnClock
    private val moveTimeout: Duration by lazy { clock.intervalSpeed }
    private lateinit var firstPlayer: NoOpPlayer
    private lateinit var secondPlayer: NoOpPlayer
    private lateinit var thirdPlayer: NoOpPlayer
    private lateinit var map: Map

    @Before
    fun setup() {
        clock = TurnClock(PrintLnLogger())
        firstPlayer = NoOpPlayer(color = Color.RED)
        secondPlayer = NoOpPlayer(color = Color.BLUE)
        thirdPlayer = NoOpPlayer(color = Color.GREEN)

        val initialCells =
            MapCellFactory.threeByFourCellsForThreePlayers(firstPlayer, secondPlayer, thirdPlayer)

        map = WritableMap(initialCells)
    }

    @Test
    fun construct_shouldThrowException_whenThereIsNoPlayer() {
        val control = kotlin.runCatching { NoOpTurnControl(map, emptySet(), moveTimeout) }

        Assert.assertThrows(IllegalArgumentException::class.java) { control.getOrThrow() }
    }

    @Test
    fun construct_shouldMakeFirstPlayerMove_whenClockChangesTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)

        Assert.assertEquals(1, firstPlayer.numberOfMoves)
    }

    @Test
    fun construct_shouldMakeFirstPlayerMoveTwice_whenClockChangesTurnTwice() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(2, firstPlayer.numberOfMoves)
    }

    @Test
    fun construct_shouldMakeSecondPlayerMove_whenClockChangesTurnTwice() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(1, secondPlayer.numberOfMoves)
    }

    @Test
    fun construct_shouldMakeFirstPlayerMoveTwiceAndSecondPlayerMoveOnce_whenClockChangesTurnThreeTimes() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(2, firstPlayer.numberOfMoves)
        Assert.assertEquals(1, secondPlayer.numberOfMoves)
    }

    @Test
    fun construct_shouldMakeFirstPlayerMoveTwiceAndSecondPlayerMoveTwice_whenClockChangesTurnFourTimes() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(2, firstPlayer.numberOfMoves)
        Assert.assertEquals(2, secondPlayer.numberOfMoves)
    }

    @Test
    fun construct_shouldMakeFirstPlayerMoveTwiceAndSecondPlayerMoveTwiceAndFirstPlayerMoveOnce_whenClockChangesTurnFiveTimes() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(3, firstPlayer.numberOfMoves)
        Assert.assertEquals(2, secondPlayer.numberOfMoves)
    }

    @Test
    fun getTurnPlayer_shouldReturnFirstPlayer_whenThereIsOnlyOnePlayer() {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)

        Assert.assertEquals(firstPlayer, control.getTurnPlayer(clock.currentTurn))
    }

    @Test
    fun getTurnPlayer_shouldReturnFirstPlayer_whenThereAreTwoPlayersAndItIsTheFirstTurn() {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)

        Assert.assertEquals(firstPlayer, control.getTurnPlayer(clock.currentTurn))
    }

    @Test
    fun getTurnPlayer_shouldReturnSecondPlayer_whenThereAreTwoPlayersAndItIsTheSecondTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)

        Assert.assertEquals(secondPlayer, control.getTurnPlayer(clock.currentTurn))
    }

    @Test
    fun getTurnPlayer_shouldReturnFirstPlayer_whenThereAreTwoPlayersAndItIsTheThirdTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(firstPlayer, control.getTurnPlayer(clock.currentTurn))
    }

    @Test
    fun makePlayerMove_shouldCallMakeMoveFromFirstPlayer_whenThereIsOnlyOnePlayer() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer), moveTimeout)

        control.makePlayerMove(clock.currentTurn)

        Assert.assertEquals(1, firstPlayer.numberOfMoves)
    }

    @Test
    fun makePlayerMove_shouldCallMakeMoveFromFirstPlayer_whenThereAreTwoPlayersAndItIsTheFirstTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)

        control.makePlayerMove(clock.currentTurn)

        Assert.assertEquals(1, firstPlayer.numberOfMoves)
    }

    @Test
    fun makePlayerMove_shouldCallMakeMoveFromSecondPlayer_whenThereAreTwoPlayersAndItIsTheSecondTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        control.makePlayerMove(clock.currentTurn)

        Assert.assertEquals(1, secondPlayer.numberOfMoves)
    }

    @Test
    fun makePlayerMove_shouldCallMakeMoveFromFirstPlayer_whenThereAreTwoPlayersAndItIsTheThirdTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)
        control.makePlayerMove(clock.currentTurn)

        Assert.assertEquals(2, firstPlayer.numberOfMoves)
    }

    @Test
    fun makePlayerMove_shouldCallMakeMoveFromFirstPlayer_whenThereAreThreePlayersAndItIsTheFirstTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer, thirdPlayer), moveTimeout)

        control.makePlayerMove(clock.currentTurn)

        Assert.assertEquals(1, firstPlayer.numberOfMoves)
    }

    @Test
    fun notifyPlayersTurn_shouldCallOnTurnFromFirstPlayer_whenThereIsOnlyOnePlayer() {
        val control = NoOpTurnControl(map, setOf(firstPlayer), moveTimeout)

        control.notifyPlayersTurn(clock.currentTurn)

        Assert.assertEquals(1, firstPlayer.turns.size)
    }

    @Test
    fun notifyPlayersTurn_shouldCallOnTurnFromFirstPlayer_whenThereAreTwoPlayersAndItIsTheFirstTurn() {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)

        control.notifyPlayersTurn(clock.currentTurn)

        Assert.assertEquals(1, firstPlayer.turns.size)
        Assert.assertEquals(1, secondPlayer.turns.size)
    }

    @Test
    fun notifyPlayersTurn_shouldCallOnTurnFromSecondPlayer_whenThereAreTwoPlayersAndItIsTheSecondTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)

        Assert.assertEquals(1, firstPlayer.turns.size)
        Assert.assertEquals(1, secondPlayer.turns.size)
    }

    @Test
    fun notifyPlayersTurn_shouldCallOnTurnFromFirstPlayer_whenThereAreTwoPlayersAndItIsTheThirdTurn() = runTest {
        val control = NoOpTurnControl(map, setOf(firstPlayer, secondPlayer), moveTimeout)
        clock.addObserver(control)

        val gameScope = CoroutineScopeBuilder.buildGameScope()
        clock.nextTurn(gameScope)
        clock.nextTurn(gameScope)

        Assert.assertEquals(2, firstPlayer.turns.size)
        Assert.assertEquals(2, secondPlayer.turns.size)
    }
}