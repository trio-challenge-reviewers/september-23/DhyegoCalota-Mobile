package dev.dhyegocalota.robots.engine.helper

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

object CoroutineScopeBuilder {
    fun buildGameScope() = CoroutineScope(Dispatchers.Default)
}