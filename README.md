# Robots!
[![Test Suite](https://github.com/dhyegocalota/robots-android-app/actions/workflows/ci.yaml/badge.svg)](https://github.com/dhyegocalota/robots-android-app/actions/workflows/ci.yaml)

- [Technical Design](assets/technical-design.pdf)
- [Zion Technical Challenge](https://zion.trio.dev/interview/technical-challenge/d4bd0f54-a6cc-408b-a11e-2407d166632a/a7a3a902-59c7-42fa-8625-0b238a13cbdd)

This is an implementation by Dhyego Calota of an exercise assessed by [Trio.dev](https://trio.dev) that allows an engineering candidate to demonstrate familiarity with Android technologies, common design patterns, coding style, and problem-solving skills in a comfortable environment.

<div align="center">
  <img src="assets/demo.gif" alt="Robots!">
</div>

## Up and Running
1. Clone this repository
2. Open the project in Android Studio
3. Run the project

## Testing
1. Open the project in Android Studio
2. Run the tests

## Changing Game Settings
1. Open the file `app/src/main/kotlin/dev/dhyegocalota/robots/android/config/ProductionFactory.kt`
2. Change the values of the variables `config`, `boundaries` and `numberOfPrizeTokens`
3. Run the project

## Author
Dhyego Calota <dhyegofernando@gmail.com>
